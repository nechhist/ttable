package websocket

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/nechhist/ttable/model"
)

var (
	urlAPIv1 = "/api/v1"

	errTournamentEmpty = "tournament is empty"

	headersOk     = handlers.AllowedHeaders([]string{"content-type"})
	methodsOk     = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	credentialsOk = handlers.AllowCredentials()
)

// getRouters ...
func (s *Service) getRouters() *mux.Router {
	router := mux.NewRouter()
	router.Use(handlers.CORS(credentialsOk, headersOk, handlers.AllowedOrigins(s.config.AllowedOrigins), methodsOk))
	router.Use(s.errorMiddleware)

	router.Handle("/metrics", promhttp.Handler()).Methods("GET")
	router.HandleFunc("/healthcheck", s.handleHealthcheck()).Methods("GET")

	apiV1 := router.PathPrefix(urlAPIv1).Subrouter()
	// example: curl -i -X GET http://localhost:15803/api/v1/index
	apiV1.HandleFunc("/", s.handleIndex()).Methods("GET")
	// example:
	apiV1.HandleFunc("/ws/tournament/{id:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		s.serveTournamentWS(w, r)
	})
	apiV1.HandleFunc("/ws/single/{id:[a-f0-9]{24}}", func(w http.ResponseWriter, r *http.Request) {
		s.serveSingleWS(w, r)
	})

	// example: curl -i -X POST http://localhost:15803/api/v1/tournament
	apiV1.HandleFunc("/tournament", s.handleUpdateTournament()).Methods("POST")
	// example: curl -i -X POST http://localhost:15803/api/v1/tournament
	apiV1.HandleFunc("/single", s.handleUpdateSingle()).Methods("POST")

	return router
}

func (s *Service) handleIndex() http.HandlerFunc {
	type IndexRequest struct {
		Status       bool `json:"status"`
		CountClients int  `json:"count_clients"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var indexRequest = IndexRequest{
			Status:       true,
			CountClients: len(s.hub.clients),
		}

		s.response(w, r, http.StatusOK, indexRequest, "")
	}
}

// serveWs handles websocket requests from the peer.
func (s *Service) serveTournamentWS(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.logger.Error("handleWS Upgrade. Error message:", err)
		return
	}

	id, err := getVariableInt(r, "id")
	if err != nil {
		s.logger.Info("handleWS getVariableInt. Error message:", err)
		s.response(w, r, http.StatusBadRequest, err, "serveWS/getVariableInt")
		return
	}

	client := &Client{
		hub:          s.hub,
		conn:         conn,
		send:         make(chan []byte, 256),
		tournamentID: id,
	}

	s.hub.register <- client
	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

// serveWs handles websocket requests from the peer.
func (s *Service) serveSingleWS(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.logger.Error("handleWS Upgrade. Error message:", err)
		return
	}

	id, err := getVariableStr(r, "id")
	if err != nil {
		s.logger.Info("handleWS getVariableInt. Error message:", err)
		s.response(w, r, http.StatusBadRequest, err, "serveWS/getVariableInt")
		return
	}

	client := &Client{
		hub:      s.hub,
		conn:     conn,
		send:     make(chan []byte, 256),
		singleID: id,
	}

	s.hub.register <- client
	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}

func (s *Service) handleHealthcheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := struct{ Status string }{"ok"}
		s.response(w, r, http.StatusOK, data, "")
	}
}

type UpdateTournamentResponse struct {
	Token      string
	Tournament model.Tournament
}

func (s *Service) handleUpdateTournament() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var updateTournamentResponse UpdateTournamentResponse

		// get json
		if err := json.NewDecoder(r.Body).Decode(&updateTournamentResponse); err != nil {
			s.logger.Info("handleUpdateTournament json.NewDecoder. Error message: %v", err)
			s.response(w, r, http.StatusBadRequest, err, "handleUpdateTournament/json.Decode")
			return
		}

		// check secret
		if err := s.checkSecret(updateTournamentResponse.Tournament.Admin, updateTournamentResponse.Token); err != nil {
			s.logger.Error("handleUpdateTournament checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleUpdateTournament/checkSecret")
			return
		}

		if updateTournamentResponse.Tournament.ID == 0 {
			s.logger.Error("handleUpdateTournament:", http.StatusUnauthorized, errTournamentEmpty)
			s.response(w, r, http.StatusUnauthorized, errTournamentEmpty, "handleUpdateTournament/emptyID")
			return
		}

		out, err := json.Marshal(updateTournamentResponse.Tournament)
		if err != nil {
			s.logger.Info("handleUpdateTournament json.Marshal. Error message: %v", err)
			s.response(w, r, http.StatusBadRequest, err, "handleUpdateTournament/json.Marshal")
			return
		}

		var send = SendToOneTournament{
			TournamentID: updateTournamentResponse.Tournament.ID,
			Data:         out,
		}

		s.hub.broadcastToOneTournament <- send
		s.response(w, r, http.StatusOK, "success", "handleUpdateTournament")
	}
}

type UpdateSingleResponse struct {
	Token  string
	Single model.Single
}

func (s *Service) handleUpdateSingle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var updateSingleResponse UpdateSingleResponse

		// get json
		if err := json.NewDecoder(r.Body).Decode(&updateSingleResponse); err != nil {
			s.logger.Info("handleUpdateSingle json.NewDecoder. Error message: %v", err)
			s.response(w, r, http.StatusBadRequest, err, "handleUpdateSingle/json.Decode")
			return
		}

		// check secret
		if err := s.checkSecret(updateSingleResponse.Single.Admin, updateSingleResponse.Token); err != nil {
			s.logger.Error("handleUpdateSingle checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleUpdateSingle/checkSecret")
			return
		}

		if updateSingleResponse.Single.ID.Hex() == "" {
			s.logger.Error("handleUpdateSingle:", http.StatusUnauthorized, errTournamentEmpty)
			s.response(w, r, http.StatusUnauthorized, errTournamentEmpty, "handleUpdateSingle/emptyID")
			return
		}

		out, err := json.Marshal(updateSingleResponse.Single)
		if err != nil {
			s.logger.Info("handleUpdateSingle json.Marshal. Error message: %v", err)
			s.response(w, r, http.StatusBadRequest, err, "handleUpdateSingle/json.Marshal")
			return
		}

		var send = SendToOneSingle{
			SingleID: updateSingleResponse.Single.ID.Hex(),
			Data:     out,
		}

		s.hub.broadcastToOneSingle <- send
		s.response(w, r, http.StatusOK, "success", "handleUpdateSingle")
	}
}

var metricRequests = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Total number of HTTP requests by status code end method.",
	},
	[]string{"handler", "code"},
)
var metricLabels = prometheus.Labels{}

// response ...
func (s *Service) response(w http.ResponseWriter, r *http.Request, code int, data interface{}, handlerName string) {
	if handlerName != "" {
		metricLabels["code"] = strconv.Itoa(code)
		metricLabels["handler"] = handlerName
		metricRequests.With(metricLabels).Inc()
	}

	w.WriteHeader(code)
	if err, ok := data.(error); ok {
		data = map[string]string{"error": err.Error()}
	}
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
	//s.logger.Debug("Response. Code:", code, ". Header: ", w.Header(), ". Data: ", data)
}
