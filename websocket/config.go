package websocket

import (
	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/kelseyhightower/envconfig"
)

// Config represents an application configuration.
type Config struct {
	LogLevel         string
	ServerPort       string
	AllowedOrigins   []string
	TokenKey         string
	TelegramTokenBot string
	TelegramChatID   string
}

// LoadConfig returns an application configuration.
func LoadConfig() (*Config, error) {
	c := &Config{
		LogLevel:         "debug",
		ServerPort:       "15803",
		TelegramTokenBot: "184883614:AAHAthCjIqTcNPNL0TEVOXkEeDxdmANs-ac",
		TelegramChatID:   "-1001060781892",

		// AllowedOrigins: []string{"http://localhost:3000"},
		// TokenKey:         "token_key",
	}
	envconfig.Process("app", c)
	if err := c.Validate(); err != nil {
		return nil, err
	}
	return c, nil
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.ServerPort, validation.Required),
		validation.Field(&c.AllowedOrigins, validation.Required),
		validation.Field(&c.TokenKey, validation.Required),
		validation.Field(&c.TelegramTokenBot, validation.Required),
		validation.Field(&c.TelegramChatID, validation.Required),
	)
}
