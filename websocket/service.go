package websocket

import (
	"fmt"
	"net/http"
	"time"

	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/shutdown"
	"gitlab.com/nechhist/ttable/pkg/telegram"
)

const (
	// Version app
	Version        = "1.0.0"
	tokenTTL int64 = 60 * 60
)

// Service ...
type Service struct {
	config     *Config
	httpServer *http.Server
	logger     log.Logger
	telegram   *telegram.Bot
	hub        *Hub
}

// New ...
func New(cfg *Config, tBot *telegram.Bot, logger log.Logger) *Service {
	s := &Service{}
	s.logger = logger
	s.telegram = tBot
	s.config = cfg
	s.httpServer = &http.Server{
		Addr:         fmt.Sprintf(":%v", cfg.ServerPort),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      s.getRouters(),
	}
	s.hub = newHub()
	go s.hub.run()
	return s
}

// ListenAndServe ...
func (s *Service) ListenAndServe() error {
	return s.httpServer.ListenAndServe()
}

// Shutdown ...
func (s *Service) Shutdown() error {
	return shutdown.Shutdown(s.httpServer, 2*time.Second)
}
