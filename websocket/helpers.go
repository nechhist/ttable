package websocket

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/nechhist/ttable/pkg/token"
)

func getVariableInt(r *http.Request, v string) (uint64, error) {
	strVar, ok := mux.Vars(r)[v]
	if !ok {
		return 0, fmt.Errorf("variable %v not found", v)
	}

	rusult, err := strconv.ParseUint(strVar, 10, 64)
	if err != nil || rusult == 0 {
		return 0, fmt.Errorf("error in the %v variable", v)
	}

	return rusult, nil
}

func getVariableStr(r *http.Request, v string) (string, error) {
	rusult, ok := mux.Vars(r)[v]
	if !ok {
		return "", fmt.Errorf("variable %v not found", v)
	}
	if rusult == "" {
		return "", fmt.Errorf("error in the %v variable - is empty", v)
	}
	return rusult, nil
}

func (s *Service) checkSecret(admin string, _token string) error {
	payload, err := token.GetPayload(_token, s.config.TokenKey, tokenTTL)
	if err != nil {
		return err
	}

	if payload != admin {
		return errors.New("token payload != tid")
	}

	return nil
}
