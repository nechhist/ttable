// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package websocket

import (
	"sync"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	mu sync.RWMutex

	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// Inbound messages from the clients of the one tournament.
	broadcastToOneTournament chan SendToOneTournament

	// Inbound messages from the clients of the one single.
	broadcastToOneSingle chan SendToOneSingle
}

// SendToOneTournament ...
type SendToOneTournament struct {
	TournamentID uint64
	Data         []byte
}

// SendToOneTournament ...
type SendToOneSingle struct {
	SingleID string
	Data     []byte
}

func newHub() *Hub {
	return &Hub{
		broadcast:                make(chan []byte),
		broadcastToOneTournament: make(chan SendToOneTournament),
		broadcastToOneSingle:     make(chan SendToOneSingle),
		register:                 make(chan *Client),
		unregister:               make(chan *Client),
		clients:                  make(map[*Client]bool),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			// fmt.Println("client register") // TODO delete
			h.mu.Lock()
			h.clients[client] = true
			h.mu.Unlock()
		case client := <-h.unregister:
			// fmt.Println("client delete") // TODO delete
			h.mu.Lock()
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
			h.mu.Unlock()
		case message := <-h.broadcast:
			h.mu.RLock()
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
			h.mu.RUnlock()
		case SendToOneTournament := <-h.broadcastToOneTournament:
			//fmt.Println("broadcastToOneTournament:", SendToOneTournament) // TODO delete
			h.mu.RLock()
			for client := range h.clients {
				if client.tournamentID == SendToOneTournament.TournamentID {
					//fmt.Println("find client broadcastToOneTournament") // TODO delete
					select {
					case client.send <- SendToOneTournament.Data:
					default:
						close(client.send)
						delete(h.clients, client)
					}
				}
			}
			h.mu.RUnlock()
		case SendToOneSingle := <-h.broadcastToOneSingle:
			//fmt.Println("SendToOneSingle:", SendToOneSingle) // TODO delete
			h.mu.RLock()
			for client := range h.clients {
				if client.singleID == SendToOneSingle.SingleID {
					//fmt.Println("find client broadcastToOneSingle") // TODO delete
					select {
					case client.send <- SendToOneSingle.Data:
					default:
						close(client.send)
						delete(h.clients, client)
					}
				}
			}
			h.mu.RUnlock()
		}
	}
}
