package main

import (
	"fmt"
	"os"

	"github.com/globalsign/mgo"
	"github.com/kidstuff/mongostore"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/mongo"
	"gitlab.com/nechhist/ttable/pkg/telegram"
	"gitlab.com/nechhist/ttable/user"
)

// Version indicates the current version of the application.
var Version = "0.9.0"

func main() {
	// config
	cfg, err := user.LoadConfig()
	if err != nil {
		fmt.Println("config error:", err)
		os.Exit(-1)
	}

	// logger
	logLevel := log.GetLevel(cfg.LogLevel)
	logger := log.New(logLevel)

	// telegram
	tBot := telegram.NewBot(telegram.Config{
		TokenBot: cfg.TelegramTokenBot,
		ChatID:   cfg.TelegramChatID,
		Prefix:   "Service User",
	})

	// store
	store, err := mongo.NewUserStore(cfg.MongoURL, mongo.UserConfig{
		NameDB:             "tt_user",
		CollectionUser:     "users",
		CollectionFeedback: "feedback",
	})
	if err != nil {
		logger.Info(err)
		os.Exit(-1)
	}

	// session
	dbSession, err := mgo.Dial(cfg.MongoURL)
	if err != nil {
		logger.Info(err)
		os.Exit(-1)
	}
	defer dbSession.Close()
	sessionStore := mongostore.NewMongoStore(
		dbSession.DB("tt_user").C("sessions"),
		86400*180,
		true,
		[]byte(cfg.SessionKey),
	)

	// new user
	s := user.New(cfg, store, sessionStore, tBot, logger)

	go func() {
		logger.Info(s.ListenAndServe())
		tBot.SendMessage("Server User is stopped")
		logger.Info("Server User is stopped")
		os.Exit(-1)
	}()

	logger.Infof("Server User is running at " + cfg.ServerPort)
	go tBot.SendMessage("Server User is running at " + cfg.ServerPort)

	if err := s.Shutdown(); err != nil {
		logger.Errorf("Error shutdown %v", err)
	}
	tBot.SendMessage("Server User is stopped")
	logger.Info("Server User is stopped")
}
