package main

import (
	"fmt"
	"os"

	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/mongo"
	"gitlab.com/nechhist/ttable/pkg/telegram"
	"gitlab.com/nechhist/ttable/tournament"
)

// Version indicates the current version of the application.
var Version = "0.9.0"

func main() {
	// config
	cfg, err := tournament.LoadConfig()
	if err != nil {
		fmt.Println("config error:", err)
		os.Exit(-1)
	}

	// logger
	logLevel := log.GetLevel(cfg.LogLevel)
	logger := log.New(logLevel)

	// telegram
	tBot := telegram.NewBot(telegram.Config{
		TokenBot: cfg.TelegramTokenBot,
		ChatID:   cfg.TelegramChatID,
		Prefix:   "Service Tournament",
	})

	// store
	store, err := mongo.NewTournamentStore(cfg.MongoURL, mongo.TournamentConfig{
		NameDB:               "tt_tournament",
		CollectionTournament: "tournaments",
		CollectionLastID:     "last_id",
		CollectionQuick:      "quick",
		CollectionSingle:     "single",
	})
	if err != nil {
		logger.Info(err)
		os.Exit(-1)
	}

	// tournament
	s := tournament.New(cfg, store, tBot, logger)
	if err := s.InitMigration(); err != nil {
		logger.Info(err)
		os.Exit(-1)
	}
	if err := s.InitIndex(); err != nil {
		logger.Info(err)
		os.Exit(-1)
	}

	go func() {
		logger.Info(s.ListenAndServe())
		tBot.SendMessage("Server Tournament is stopped")
		logger.Infof("Server Tournament is stopped")
		os.Exit(-1)
	}()

	logger.Infof("Server Tournament is running at " + cfg.ServerPort)
	go tBot.SendMessage("Server Tournament is running at " + cfg.ServerPort)

	if err := s.Shutdown(); err != nil {
		logger.Errorf("Error shutdown %v", err)
	}
	tBot.SendMessage("Server Tournament is stopped")
	logger.Infof("Server Tournament is stopped")
}
