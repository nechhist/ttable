package main

import (
	"fmt"
	"os"

	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/telegram"
	"gitlab.com/nechhist/ttable/websocket"
)

// Version indicates the current version of the application.
var Version = "0.9.0"

func main() {
	// config
	cfg, err := websocket.LoadConfig()
	if err != nil {
		fmt.Println("config error:", err)
		os.Exit(-1)
	}

	// logger
	logLevel := log.GetLevel(cfg.LogLevel)
	logger := log.New(logLevel)

	// telegram
	tBot := telegram.NewBot(telegram.Config{
		TokenBot: cfg.TelegramTokenBot,
		ChatID:   cfg.TelegramChatID,
		Prefix:   "Service websocket",
	})

	// websocket
	s := websocket.New(cfg, tBot, logger)

	go func() {
		logger.Info(s.ListenAndServe())
		tBot.SendMessage("Server Websocket is stopped")
		logger.Info("Server Websocket is stopped")
		os.Exit(-1)
	}()

	logger.Infof("Server Websocket is running at " + cfg.ServerPort)
	go tBot.SendMessage("Server Websocket is running at " + cfg.ServerPort)

	if err := s.Shutdown(); err != nil {
		logger.Errorf("Error shutdown %v", err)
	}
	tBot.SendMessage("Server Websocket is stopped")
	logger.Info("Server Websocket is stopped")
}
