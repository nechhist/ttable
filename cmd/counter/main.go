package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/nechhist/ttable/counter"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/telegram"
)

// Version indicates the current version of the application.
var Version = "0.9.0"

func main() {
	// config
	cfg, err := counter.LoadConfig()
	if err != nil {
		fmt.Println("config error:", err)
		os.Exit(-1)
	}

	// logger
	logLevel := log.GetLevel(cfg.LogLevel)
	logger := log.New(logLevel)

	// telegram
	tBot := telegram.NewBot(telegram.Config{
		TokenBot: cfg.TelegramTokenBot,
		ChatID:   cfg.TelegramChatID,
		Prefix:   "Service Counter",
	})

	// store
	store := counter.NewStore()
	if err := store.Init(cfg.PathFile); err != nil {
		logger.Error(err)
		tBot.SendMessage("store init error: " + err.Error())
	}

	// counter
	s := counter.New(cfg, store, tBot, logger)

	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	go func() {
		err := s.RunSavingAndCleaning(ctx)
		logger.Error(err)
		tBot.SendMessage("RunSavingAndCleaning: " + err.Error())
		os.Exit(-1)
	}()

	go func() {
		logger.Info(s.ListenAndServe())
		logger.Infof("Server Counter is stopped")
		tBot.SendMessage("Server Counter is stopped")
		os.Exit(-1)
	}()

	logger.Infof("Server Counter is running at " + cfg.ServerPort)
	go tBot.SendMessage("Server Counter is running at " + cfg.ServerPort)

	if err := s.Shutdown(); err != nil {
		logger.Errorf("Error shutdown %v", err)
	}
	tBot.SendMessage("Server Counter is stopped")
	logger.Infof("Server Counter is stopped")
}
