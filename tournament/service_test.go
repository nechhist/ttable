package tournament

import (
	"net/http"
	"testing"

	"gitlab.com/nechhist/ttable/pkg/antispam"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/mongo"
)

// MongoServer //////////////////////////////
func getTestServer(t *testing.T) Service {
	t.Helper()

	s := Service{}
	s.logger = log.New(log.GetLevel("debug"))
	s.config = &Config{
		TokenKey:       "token_key",
		AllowedOrigins: []string{"*"},
	}

	store, err := mongo.NewTournamentStore("mongodb://admin:admin@localhost:27017", mongo.TournamentConfig{
		NameDB:               "test_tt",
		CollectionTournament: "test_tournament",
		CollectionLastID:     "test_last_id",
		CollectionSingle:     "test_single",
		CollectionQuick:      "test_quick",
	})
	if err != nil {
		t.Fatal(err)
	}

	if err := store.InitMigration(); err != nil {
		t.Fatal(err)
	}

	s.store = store
	s.httpServer = &http.Server{
		Handler: s.getHandlers(),
	}
	s.spamStore = antispam.NewStore(0)
	s.indexStore = newIndexStore()

	return s
}
