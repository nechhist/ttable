package tournament

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/token"
)

// TournamentToWSRespons ...
type TournamentToWSResponse struct {
	Token      string            `json:"token"`
	Tournament *model.Tournament `json:"tournament"`
}

func (s *Service) postTournamentToWS(t *model.Tournament) {
	if t.ID == 0 {
		return
	}

	_token, err := token.Create(t.Admin, s.config.TokenKey)
	if err != nil {
		s.logger.Error("handleAuthorization token create error:", http.StatusInternalServerError, err)
		return
	}

	tournamentToWSResponse := TournamentToWSResponse{
		Token:      _token,
		Tournament: t,
	}

	data, err := json.Marshal(tournamentToWSResponse)
	if err != nil {
		s.logger.Error("postTournamentToWS json.Marshal:", err)
		return
	}

	req, err := http.NewRequest("POST", s.config.WebsocketAddr+"/tournament", bytes.NewBuffer(data))
	if err != nil {
		s.logger.Error("postTournamentToWS http.NewRequest:", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 1}
	resp, err := client.Do(req)
	if err != nil {
		s.logger.Error("postTournamentToWS client.Do:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		s.logger.Error("postTournamentToWS StatusCode:", resp.StatusCode)
		return
	}
	s.logger.Debug("postTournamentToWS OK", tournamentToWSResponse)
}

// SingleToWSResponse ...
type SingleToWSResponse struct {
	Token  string        `json:"token"`
	Single *model.Single `json:"single"`
}

func (s *Service) postSingleToWS(single *model.Single) {
	if single.ID.Hex() == "" {
		s.logger.Error("postSingleToWS error: single.ID empty")
		return
	}

	_token, err := token.Create(single.Admin, s.config.TokenKey)
	if err != nil {
		s.logger.Error("postSingleToWS token create error:", err)
		return
	}

	singleToWSResponse := SingleToWSResponse{
		Token:  _token,
		Single: single,
	}

	data, err := json.Marshal(singleToWSResponse)
	if err != nil {
		s.logger.Error("postSingleToWS json.Marshal:", err)
		return
	}

	req, err := http.NewRequest("POST", s.config.WebsocketAddr+"/single", bytes.NewBuffer(data))
	if err != nil {
		s.logger.Error("postSingleToWS http.NewRequest:", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 1}
	resp, err := client.Do(req)
	if err != nil {
		s.logger.Error("postSingleToWS client.Do:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		s.logger.Error("postSingleToWS StatusCode:", resp.StatusCode)
		return
	}
	s.logger.Debug("postSingleToWS OK")
}
