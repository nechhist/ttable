package tournament

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/antispam"
	"gitlab.com/nechhist/ttable/pkg/mongo"
)

func (s *Service) handleTournamentGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableInt(r, "id")
		if err != nil {
			s.logger.Info("handleTournamentGet getVariableInt:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentGet/getVariableInt")
			return
		}

		tournament, err := s.store.FindTournamentByID(id)
		if err != nil {
			s.logger.Info("handleTournamentGet findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleTournamentGet/findByID")
			return
		}
		s.response(w, r, http.StatusOK, tournament, "handleTournamentGet")
	}
}

// TournamentRequest ...
type TournamentRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	Name        string `json:"name"`
	Game        string `json:"game"`
	Description string `json:"description"`
	Logo        string `json:"logo"`
}

func (s *Service) handleTournamentCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// antispam
		if err := s.spamStore.Add(r.RemoteAddr); err != nil {
			if err == antispam.ErrIPExists {
				s.logger.Info("handleTournamentCreate anti_spam add ErrIPExists error:", http.StatusUnprocessableEntity, errResponseSpam)
				s.response(w, r, http.StatusUnprocessableEntity, errResponseSpam, "TournamentCreate/ErrIPExists")
			} else {
				s.logger.Info("handleTournamentCreate anti_spam add error:", http.StatusUnprocessableEntity, err)
				s.response(w, r, http.StatusUnprocessableEntity, err, "TournamentCreate/spamStore.Add")
			}
			return
		}

		// get json
		var tournamentRequest TournamentRequest
		if err := json.NewDecoder(r.Body).Decode(&tournamentRequest); err != nil {
			s.logger.Info("handleTournamentCreate json Decode:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentCreate/json.Decode")
			return
		}

		if tournamentRequest.Admin == "" || tournamentRequest.Token == "" {
			s.logger.Error("handleTournamentCreate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleTournamentCreate/empty")
			return
		}

		// check secret
		if err := s.checkSecret(tournamentRequest.Admin, tournamentRequest.Token); err != nil {
			s.logger.Error("handleTournamentCreate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleTournamentCreate/checkSecret")
			return
		}

		tournament := &model.Tournament{}
		tournament.Name = tournamentRequest.Name
		tournament.Admin = tournamentRequest.Admin
		tournament.Game = tournamentRequest.Game
		tournament.Description = tournamentRequest.Description
		tournament.Logo = tournamentRequest.Logo

		tournament.Seasons = make(map[uint64]model.Season)

		tournament.Format()
		tournament.BeforeCreate()
		if err := tournament.Validate(); err != nil {
			s.logger.Error("handleTournamentCreate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentCreate/BeforeCreate")
			return
		}

		if err := s.store.CreateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentCreate createTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentCreate/createTournament")
			return
		}

		go s.telegram.SendMessage("Create Tournament " + tournament.Name + "(id: " + strconv.Itoa(int(tournament.ID)) + ") admin: " + tournament.Admin)
		s.response(w, r, http.StatusCreated, tournament, "handleTournamentCreate")
	}
}

func (s *Service) handleTournamentUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var tournamentRequest TournamentRequest
		if err := json.NewDecoder(r.Body).Decode(&tournamentRequest); err != nil {
			s.logger.Error("handleTournamentUpdate json Decode:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentUpdate/json.Decode")
			return
		}

		if tournamentRequest.Admin == "" || tournamentRequest.Token == "" {
			s.logger.Error("handleTournamentUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleTournamentUpdate/empty")
			return
		}

		id, err := getVariableInt(r, "id")
		if err != nil {
			s.logger.Info("handleTournamentUpdate getVariableInt:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(tournamentRequest.Admin, tournamentRequest.Token); err != nil {
			s.logger.Info("handleTournamentUpdate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleTournamentUpdate/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(id)
		if err != nil {
			s.logger.Info("handleTournamentUpdate findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleTournamentUpdate/findByID")
			return
		}

		if tournament.Admin != tournamentRequest.Admin {
			s.logger.Info("handleTournamentUpdate: admin tournament not equal", http.StatusBadRequest)
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleTournamentUpdate/admin")
			return
		}

		tournament.Name = tournamentRequest.Name
		tournament.Admin = tournamentRequest.Admin
		tournament.Game = tournamentRequest.Game
		tournament.Description = tournamentRequest.Description
		tournament.Logo = tournamentRequest.Logo

		tournament.Format()
		tournament.BeforeUpdate()
		if err := tournament.Validate(); err != nil {
			s.logger.Error("handleTournamentUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentUpdate/Validate")
			return
		}

		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentUpdate updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentUpdate/updateTournament")
			return
		}

		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusOK, tournament, "handleTournamentUpdate/updateTournament")
	}
}

// SeasonRequest ...
type SeasonRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	Name        string `json:"name"`
	TimeAction  string `json:"time_action"`
	Table       string `json:"table"`
	ScoreEnable uint64 `json:"score_enable"`
	Status      uint64 `json:"-"`
}

func (s *Service) handleTournamentSeasonCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var seasonRequest SeasonRequest
		if err := json.NewDecoder(r.Body).Decode(&seasonRequest); err != nil {
			s.logger.Error("handleTournamentSeasonCreate json Decoder:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonCreate/json.Decode")
			return
		}

		if seasonRequest.Admin == "" || seasonRequest.Token == "" {
			s.logger.Error("handleTournamentSeasonCreate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleTournamentSeasonCreate/empty")
			return
		}

		tid, err := getVariableInt(r, "tid")
		if err != nil {
			s.logger.Error("handleTournamentSeasonCreate getVariableInt tid:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonCreate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(seasonRequest.Admin, seasonRequest.Token); err != nil {
			s.logger.Error("handleTournamentSeasonCreate checkSecret:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleTournamentSeasonCreate/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(tid)
		if err != nil {
			s.logger.Error("handleTournamentSeasonCreate findByID:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusNotFound, err, "handleTournamentSeasonCreate/findByID")
			return
		}

		if tournament.Admin != seasonRequest.Admin {
			s.logger.Info("handleTournamentSeasonCreate: ", http.StatusBadRequest, " dmin tournament not equal ", tournament.Admin, " != ", seasonRequest.Admin)
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleTournamentSeasonCreate/admin")
			return
		}

		season := model.Season{}
		season.Name = seasonRequest.Name
		season.TimeAction = seasonRequest.TimeAction
		season.Table = seasonRequest.Table
		season.ScoreEnable = seasonRequest.ScoreEnable
		season.Units = make(map[string]model.Unit)
		season.Times = make(map[string]model.Time)
		season.Scores = make(map[string]model.Score)

		season = season.Format()
		season = season.BeforeCreate()
		if err := season.Validate(); err != nil {
			s.logger.Error("handleTournamentSeasonCreate Validate:", http.StatusBadRequest, err, "handleSeasonCreate")
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonCreate/Validate")
			return
		}

		sid := uint64(len(tournament.Seasons) + 1)
		if tournament.Seasons == nil {
			tournament.Seasons = make(map[uint64]model.Season)
		}
		tournament.Seasons[sid] = season
		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentSeasonCreate updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentSeasonCreate/updateTournament")
			return
		}

		err = s.indexStore.add(TypeIndexTournament, strconv.Itoa(int(tournament.ID))+"/"+strconv.Itoa(int(sid)), tournament.Name+" ("+seasonRequest.Name+")", tournament.Admin, season.TimeCreate)
		if err != nil {
			s.logger.Error("handleTournamentSeasonCreate indexStore.add:", err)
		}
		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusCreated, tournament, "handleTournamentSeasonCreate")
	}
}

func (s *Service) handleTournamentSeasonUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var seasonRequest SeasonRequest
		if err := json.NewDecoder(r.Body).Decode(&seasonRequest); err != nil {
			s.logger.Error("handleTournamentSeasonUpdate json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonUpdate/json.Decode")
			return
		}

		if seasonRequest.Admin == "" || seasonRequest.Token == "" {
			s.logger.Error("handleTournamentSeasonUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleTournamentSeasonUpdate/empty")
			return
		}

		tid, err := getVariableInt(r, "tid")
		if err != nil {
			s.logger.Error("handleTournamentSeasonUpdate getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonUpdate/getVariableInt")
			return
		}

		sid, err := getVariableInt(r, "sid")
		if err != nil {
			s.logger.Error("handleTournamentSeasonUpdate getVariableInt sid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(seasonRequest.Admin, seasonRequest.Token); err != nil {
			s.logger.Error("handleTournamentSeasonUpdate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleTournamentSeasonUpdate/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(tid)
		if err != nil {
			s.logger.Error("handleTournamentSeasonUpdate findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleTournamentSeasonUpdate/findByID")
			return
		}

		if tournament.Admin != seasonRequest.Admin {
			s.logger.Info("handleTournamentSeasonUpdate:", http.StatusBadRequest, "admin tournament not equal")
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleTournamentSeasonUpdate/admin")
			return
		}

		season, ok := tournament.Seasons[sid]
		if !ok {
			s.logger.Error("handleTournamentSeasonUpdate:", http.StatusNotFound, mongo.ErrSeasonNotFound)
			s.response(w, r, http.StatusNotFound, mongo.ErrSeasonNotFound, "handleTournamentSeasonUpdate/noSeason")
			return
		}

		season.Name = seasonRequest.Name
		season.TimeAction = seasonRequest.TimeAction
		season.Table = seasonRequest.Table
		season.ScoreEnable = seasonRequest.ScoreEnable

		season = season.Format()
		season = season.BeforeUpdate()
		if err := season.Validate(); err != nil {
			s.logger.Error("handleTournamentSeasonUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSeasonUpdate/Validate")
			return
		}

		tournament.Seasons[sid] = season
		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentSeasonUpdate updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentSeasonUpdate/updateTournament")
			return
		}

		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusOK, tournament, "handleTournamentSeasonUpdate")
	}
}

// TournamentUnitRequest ...
type TournamentUnitRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	UnitID string `json:"unit_id"`
	Value  string `json:"value"`
	Score  uint64 `json:"score"`
	Logo   string `json:"-"`
}

func (s *Service) handleTournamentUnitUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var unitRequest TournamentUnitRequest
		if err := json.NewDecoder(r.Body).Decode(&unitRequest); err != nil {
			s.logger.Error("handleSetUnit json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetUnit/json.Decode")
			return
		}

		if unitRequest.Admin == "" || unitRequest.Token == "" {
			s.logger.Error("handleSetUnit:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSetUnit/empty")
			return
		}

		tid, err := getVariableInt(r, "tid")
		if err != nil {
			s.logger.Error("handleSetUnit getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetUnit/getVariableInt")
			return
		}

		sid, err := getVariableInt(r, "sid")
		if err != nil {
			s.logger.Error("handleSetUnit getVariableInt sid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetUnit/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(unitRequest.Admin, unitRequest.Token); err != nil {
			s.logger.Error("handleSetUnit checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSetUnit/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(tid)
		if err != nil {
			s.logger.Error("handleSetUnit findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleSetUnit/findByID")
			return
		}

		if tournament.Admin != unitRequest.Admin {
			s.logger.Info("handleSetUnit:", http.StatusBadRequest, "admin tournament not equal")
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleSetUnit/admin")
			return
		}

		season, ok := tournament.Seasons[sid]
		if !ok {
			s.logger.Error("handleSetUnit:", http.StatusNotFound, mongo.ErrSeasonNotFound)
			s.response(w, r, http.StatusNotFound, mongo.ErrSeasonNotFound, "handleSetUnit/noSeason")
			return
		}

		unit := model.Unit{}
		_, _, _, _, err = getUnitParams(unitRequest.UnitID)
		if err != nil {
			s.logger.Error("handleSetUnit getUnitParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetUnit/getUnitParams")
			return
		}
		unit.Value = unitRequest.Value
		unit.Score = unitRequest.Score

		unit = unit.Format()
		unit = unit.BeforeUpdate()
		if err = unit.Validate(); err != nil {
			s.logger.Error("handleSetUnit Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetUnit/Validate")
			return
		}

		season.Units[unitRequest.UnitID] = unit
		season = season.BeforeUpdate()
		tournament.Seasons[sid] = season
		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleSetUnit updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSetUnit/updateTournament")
			return
		}

		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusOK, tournament, "handleSetUnit")
	}
}

// TournamentTimeRequest ...
type TournamentTimeRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	TimeID string `json:"time_id"`
	Value  string `json:"value"`
}

func (s *Service) handleTournamentTimeUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var timeRequest TournamentTimeRequest
		if err := json.NewDecoder(r.Body).Decode(&timeRequest); err != nil {
			s.logger.Error("handleTournamentTimeUpdate json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentTimeUpdate/json.Decode")
			return
		}

		if timeRequest.Admin == "" || timeRequest.Token == "" {
			s.logger.Error("handleTournamentTimeUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleTournamentTimeUpdate/admin")
			return
		}

		tid, err := getVariableInt(r, "tid")
		if err != nil {
			s.logger.Error("handleTournamentTimeUpdate getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentTimeUpdate/getVariableInt")
			return
		}

		sid, err := getVariableInt(r, "sid")
		if err != nil {
			s.logger.Error("handleTournamentTimeUpdate getVariableInt sid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentTimeUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(timeRequest.Admin, timeRequest.Token); err != nil {
			s.logger.Error("handleTournamentTimeUpdate checkSecret:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleTournamentTimeUpdate/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(tid)
		if err != nil {
			s.logger.Error("handleTournamentTimeUpdate findByID:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusNotFound, err, "handleTournamentTimeUpdate/findByID")
			return
		}

		if tournament.Admin != timeRequest.Admin {
			s.logger.Info("handleTournamentTimeUpdate:", http.StatusBadRequest, "admin tournament not equal")
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleTournamentTimeUpdate/noAdmin")
			return
		}

		season, ok := tournament.Seasons[sid]
		if !ok {
			s.logger.Error("handleTournamentTimeUpdate:", http.StatusUnprocessableEntity, mongo.ErrSeasonNotFound)
			s.response(w, r, http.StatusNotFound, mongo.ErrSeasonNotFound, "handleTournamentTimeUpdate/noSeasons")
			return
		}

		_time := model.Time{}
		_, _, _, _, err = getUnitParams(timeRequest.TimeID)
		if err != nil {
			s.logger.Error("handleTournamentTimeUpdate getUnitParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentTimeUpdate/getUnitParams")
			return
		}
		_time.Value = timeRequest.Value

		_time = _time.Format()
		_time = _time.BeforeUpdate()
		if err = _time.Validate(); err != nil {
			s.logger.Error("handleTournamentTimeUpdate Validate:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentTimeUpdate/Validate")
			return
		}

		season.Times[timeRequest.TimeID] = _time
		season = season.BeforeUpdate()
		tournament.Seasons[sid] = season
		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentTimeUpdate updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentTimeUpdate/updateTournament")
			return
		}

		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusOK, tournament, "handleTournamentTimeUpdate")
	}
}

// TournamentScoreRequest ...
type TournamentScoreRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	ScoreID string `json:"score_id"`
	Score1  uint64 `json:"score1"`
	Score2  uint64 `json:"score2"`
}

func (s *Service) handleTournamentScoreUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var scoreRequest TournamentScoreRequest
		if err := json.NewDecoder(r.Body).Decode(&scoreRequest); err != nil {
			s.logger.Error("handleSetTime json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetTime/json.Decode")
			return
		}

		if scoreRequest.Admin == "" || scoreRequest.Token == "" {
			s.logger.Error("handleSetTime:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSetTime/admin")
			return
		}

		tid, err := getVariableInt(r, "tid")
		if err != nil {
			s.logger.Error("handleSetTime getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetTime/getVariableInt")
			return
		}

		sid, err := getVariableInt(r, "sid")
		if err != nil {
			s.logger.Error("handleSetTime getVariableInt sid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSetTime/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(scoreRequest.Admin, scoreRequest.Token); err != nil {
			s.logger.Error("handleSetTime checkSecret:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSetTime/checkSecret")
			return
		}

		tournament, err := s.store.FindTournamentByID(tid)
		if err != nil {
			s.logger.Error("handleSetTime findByID:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusNotFound, err, "handleSetTime/findByID")
			return
		}

		if tournament.Admin != scoreRequest.Admin {
			s.logger.Info("handleSetTime:", http.StatusBadRequest, "admin tournament not equal")
			s.response(w, r, http.StatusBadRequest, "admin tournament not equal", "handleSetTime/noAdmin")
			return
		}

		season, ok := tournament.Seasons[sid]
		if !ok {
			s.logger.Error("handleSetTime:", http.StatusUnprocessableEntity, mongo.ErrSeasonNotFound)
			s.response(w, r, http.StatusNotFound, mongo.ErrSeasonNotFound, "handleSetTime/noSeasons")
			return
		}

		score := model.Score{}
		//unit1, unit2, _type, up, err := getUnitParams(scoreRequest.ScoreID)
		_, _, _, _, err = getUnitParams(scoreRequest.ScoreID)
		if err != nil {
			s.logger.Error("handleTournamentScoreUpdate getScoreParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentScoreUpdate/getScoreParams")
			return
		}
		score.Score1 = scoreRequest.Score1
		score.Score2 = scoreRequest.Score2

		score = score.Format()
		score = score.BeforeUpdate()
		if err = score.Validate(); err != nil {
			s.logger.Error("handleTournamentScoreUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentScoreUpdate/Validate")
			return
		}

		season.Scores[scoreRequest.ScoreID] = score

		season = season.BeforeUpdate()
		tournament.Seasons[sid] = season
		if err := s.store.UpdateTournament(tournament); err != nil {
			s.logger.Error("handleTournamentScoreUpdate updateTournament:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleTournamentScoreUpdate/updateTournament")
			return
		}

		s.postTournamentToWS(tournament)
		s.response(w, r, http.StatusOK, tournament, "handleTournamentScoreUpdate")
	}
}

// MyRequest ...
type MyRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`
}

// MyResponse ...
type MyResponse struct {
	Tournaments map[uint64]string `json:"tournaments"`
	Singles     map[string]string `json:"singles"`
}

func (s *Service) handleMy() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var myRequest MyRequest
		if err := json.NewDecoder(r.Body).Decode(&myRequest); err != nil {
			s.logger.Error("handleMy json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleMy/json.Decode")
			return
		}

		if myRequest.Admin == "" || myRequest.Token == "" {
			s.logger.Error("handleMy:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleMy/empty")
			return
		}

		// check secret
		if err := s.checkSecret(myRequest.Admin, myRequest.Token); err != nil {
			s.logger.Error("handleMy checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleMy/checkSecret")
			return
		}

		// get myTournaments
		myTournaments, err := s.store.FindAllTournamentsByAdmin(myRequest.Admin)
		if err != nil {
			s.logger.Error("handleMy findAllByAdmin:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleMy/FindAllTournamentsByAdmin")
			return
		}

		// get myTournaments
		mySingles, err := s.store.FindAllSinglesByAdmin(myRequest.Admin)
		if err != nil {
			s.logger.Error("handleMy FindAllSinglesByAdmin:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleMy/FindAllSinglesByAdmin")
			return
		}

		myResponse := MyResponse{
			Tournaments: myTournaments,
			Singles:     mySingles,
		}

		s.response(w, r, http.StatusOK, myResponse, "handleMy")
	}
}
