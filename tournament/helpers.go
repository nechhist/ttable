package tournament

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

func getUnitID(row, col, third, loser uint64) string {
	return fmt.Sprintf("%d_%d_%d_%d", row, col, third, loser)
}

// unit: col, row, loser, third
// time: col, row, loser, third
// score: unit1, unit2, type, up
func getUnitParams(id string) (param1, param2, param3, param4 uint64, err error) {
	arr := strings.Split(id, "_")
	if len(arr) != 4 {
		err = fmt.Errorf("unit ID is failed, split len %v", len(arr))
		return
	}

	param1, err = strconv.ParseUint(arr[0], 10, 64)
	if err != nil {
		err = fmt.Errorf("unit ID is failed, param1. %v", err)
		return
	}
	param2, err = strconv.ParseUint(arr[1], 10, 64)
	if err != nil {
		err = fmt.Errorf("unit ID is failed, param2. %v", err)
		return
	}
	param3, err = strconv.ParseUint(arr[2], 10, 64)
	if err != nil {
		err = fmt.Errorf("unit ID is failed, param3. %v", err)
		return
	}
	param4, err = strconv.ParseUint(arr[3], 10, 64)
	if err != nil {
		err = fmt.Errorf("unit ID is failed, param4. %v", err)
		return
	}

	return
}

func getVariableInt(r *http.Request, v string) (uint64, error) {
	strVar, ok := mux.Vars(r)[v]
	if !ok {
		return 0, fmt.Errorf("variable %v not found", v)
	}

	rusult, err := strconv.ParseUint(strVar, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("error in the %v variable, err: %v", v, err)
	}

	if rusult == 0 {
		return 0, fmt.Errorf("error in the %v variable, value is 0", v)
	}

	return rusult, nil
}

func getVariableStr(r *http.Request, v string) (string, error) {
	rusult, ok := mux.Vars(r)[v]
	if !ok {
		return "", fmt.Errorf("variable %v not found", v)
	}
	if rusult == "" {
		return "", fmt.Errorf("error in the %v variable - is empty", v)
	}
	return rusult, nil
}
