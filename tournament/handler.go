package tournament

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// errResponseSpam = fmt.Errorf("too many registrations from this IP address, wait %d min", delayAntispamSec)
	errResponseSpam        = fmt.Errorf("Слишком много регистраций с данного IP, подождите %d сек.", delayAntispamSec)
	errNotEnoughParameters = errors.New("not enough parameters")
)

var (
	// Where ORIGIN_ALLOWED is like `scheme://dns[:port]`, or `*` (insecure)
	headersOk     = handlers.AllowedHeaders([]string{"content-type"})
	methodsOk     = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	credentialsOk = handlers.AllowCredentials()
)

// getHandlers ...
func (s *Service) getHandlers() *mux.Router {
	router := mux.NewRouter()
	router.Use(handlers.CORS(credentialsOk, headersOk, handlers.AllowedOrigins(s.config.AllowedOrigins), methodsOk))
	router.Handle("/metrics", promhttp.Handler()).Methods("GET")
	router.HandleFunc("/healthcheck", s.handleHealthCheck()).Methods("GET")

	// API
	apiV1 := router.PathPrefix("/api/v1").Subrouter()
	apiV1.Use(s.accessMiddleware)
	apiV1.Use(s.errorMiddleware)

	// INDEX
	// example: curl -i -X GET http://localhost:15800/api/v1/index
	apiV1.HandleFunc("/index", s.handleIndex()).Methods("GET")

	// TOURNAMENT
	// example: curl -i -X GET http://localhost:15800/api/v1/tournament/1
	apiV1.HandleFunc("/tournament/{id}", s.handleTournamentGet()).Methods("GET")
	// example:
	apiV1.HandleFunc("/tournament", s.handleTournamentCreate()).Methods("POST")
	// example: curl -i -X POST http://localhost:15800/api/v1/tournament/1
	apiV1.HandleFunc("/tournament/{id:[0-9]+}", s.handleTournamentUpdate()).Methods("POST")
	// example: curl -i -X POST -d "{\"name\":\"Season 1\",\"time_action\":\"12.12.2020\",\"table\":\"tree_4\"}" http://localhost:15800/api/v1/season
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season", s.handleTournamentSeasonCreate()).Methods("POST")
	// example: curl -i -X POST -d "{\"name\":\"Season 1\",\"time_action\":\"12.12.2020\",\"table\":\"tree_4\"}" http://localhost:15800/api/v1/season
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}", s.handleTournamentSeasonUpdate()).Methods("POST")
	// example: curl -i -X POST -d "{\"col\":1,\"row\":1,\"third\":0,\"loser\":0,\"value\":\"unit 1\"}" http://localhost:15800/api/v1/unit
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/unit", s.handleTournamentUnitUpdate()).Methods("POST")
	// example:
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/time", s.handleTournamentTimeUpdate()).Methods("POST")
	// example:
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/score", s.handleTournamentScoreUpdate()).Methods("POST")

	// SINGLE
	// example: curl -i -X GET http://localhost:15800/api/v1/single/aq12hsjdhas7d6sj33
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}", s.handleSingleGet()).Methods("GET")
	// example:
	apiV1.HandleFunc("/single", s.handleSingleCreate()).Methods("POST")
	// example: curl -i -X POST http://localhost:15800/api/v1/single/1aq12hsjdhas7d6sj33
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}", s.handleSingleUpdate()).Methods("POST")
	// example: curl -i -X POST -d "{\"col\":1,\"row\":1,\"third\":0,\"loser\":0,\"value\":\"unit 1\"}" http://localhost:15800/api/v1/unit
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/unit", s.handleSingleUnitUpdate()).Methods("POST")
	// example:
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/time", s.handleSingleTimeUpdate()).Methods("POST")
	// example:
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/score", s.handleSingleScoreUpdate()).Methods("POST")
	// example: curl -i -X GET http://localhost:15800/api/v1/my_singles
	// apiV1.HandleFunc("/single/my", s.handleMySingles()).Methods("POST")

	// QUICK
	//

	// example: curl -i -X GET http://localhost:15800/api/v1/my_tournaments
	apiV1.HandleFunc("/my", s.handleMy()).Methods("POST")

	// DUMMY
	apiV1.HandleFunc("/tournament", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{id:[0-9]+}", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/unit", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/time", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/tournament/{tid:[0-9]+}/season/{sid:[0-9]+}/score", s.handleDummy).Methods("OPTIONS")

	apiV1.HandleFunc("/single", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/unit", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/time", s.handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/single/{id:[a-f0-9]{24}}/score", s.handleDummy).Methods("OPTIONS")

	apiV1.HandleFunc("/my", s.handleDummy).Methods("OPTIONS")

	return router
}

func (s *Service) handleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		indexTournaments := s.indexStore.getAll()
		s.response(w, r, http.StatusOK, indexTournaments, "")
	}
}

// handleHealthCheck ...
func (s *Service) handleHealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := struct{ Status string }{"ok"}
		s.response(w, r, http.StatusOK, data, "")
	}
}

// handleDummy
func (s *Service) handleDummy(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello Dummy!")
}

var metricRequests = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Total number of HTTP requests by status code end method.",
	},
	[]string{"handler", "code"},
)
var metricLabels = prometheus.Labels{}

// response ...
func (s *Service) response(w http.ResponseWriter, r *http.Request, code int, data interface{}, handlerName string) {
	if handlerName != "" {
		metricLabels["code"] = strconv.Itoa(code)
		metricLabels["handler"] = handlerName
		metricRequests.With(metricLabels).Inc()
	}

	w.WriteHeader(code)
	if err, ok := data.(error); ok {
		data = map[string]string{"error": err.Error()}
	}
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
	//s.logger.Debugf("response. code: %v header:%v, data:%v", code, w.Header(), data)
}
