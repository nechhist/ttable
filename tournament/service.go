package tournament

import (
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"time"

	"gitlab.com/nechhist/ttable/pkg/antispam"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/shutdown"
	"gitlab.com/nechhist/ttable/pkg/telegram"
	"gitlab.com/nechhist/ttable/pkg/token"
)

const (
	delayAntispamSec int64 = 60
	tokenTTL         int64 = 60 * 60 * 24
)

// Service ...
type Service struct {
	config     *Config
	httpServer *http.Server
	logger     log.Logger
	store      Store
	indexStore *indexStore
	spamStore  *antispam.Store
	telegram   *telegram.Bot
}

// New ...
func New(cfg *Config, store Store, telegramBot *telegram.Bot, logger log.Logger) *Service {
	s := &Service{}
	s.config = cfg
	s.httpServer = &http.Server{
		Addr:         fmt.Sprintf(":%v", cfg.ServerPort),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      s.getHandlers(),
	}
	s.logger = logger
	s.store = store

	s.indexStore = newIndexStore()
	go s.indexStore.runGC()

	s.spamStore = antispam.NewStore(delayAntispamSec)
	go s.spamStore.RunGC()

	s.telegram = telegramBot

	return s
}

// ListenAndServe ...
func (s *Service) ListenAndServe() error {
	return s.httpServer.ListenAndServe()
}

// Shutdown ...
func (s *Service) Shutdown() error {
	return shutdown.Shutdown(s.httpServer, 2*time.Second)
}

// InitIndex ...
func (s *Service) InitIndex() error {
	var ii = make([]indexRecord, 0)

	// add tournaments
	tt, err := s.store.FindAllTournaments(0, limitIndexRecords/2, "time_create", true)
	if err != nil {
		return err
	}
	for _, tournament := range tt {
		for i, season := range tournament.Seasons {
			ii = append(ii, indexRecord{
				Type:  TypeIndexTournament,
				ID:    strconv.Itoa(int(tournament.ID)) + "/" + strconv.Itoa(int(i)),
				Name:  tournament.Name + " (" + season.Name + ")",
				Admin: tournament.Admin,
				Time:  season.TimeCreate,
			})
		}
	}

	// seasons
	ss, err := s.store.FindAllSingles(0, limitIndexRecords/2, "time_create", true)
	if err != nil {
		return err
	}
	for _, s := range ss {
		ii = append(ii, indexRecord{
			Type:  TypeIndexSingle,
			ID:    s.ID.Hex(),
			Name:  s.Name,
			Admin: s.Admin,
			Time:  s.TimeCreate,
		})
	}

	sort.Slice(ii[:], func(i, j int) bool {
		return ii[i].Time < ii[j].Time
	})

	for _, i := range ii {
		s.indexStore.add(i.Type, i.ID, i.Name, i.Admin, i.Time)
	}

	return nil
}

// InitMigration ...
func (s *Service) InitMigration() error {
	return s.store.InitMigration()
}

func (s *Service) checkSecret(admin, requestToken string) error {
	payload, err := token.GetPayload(requestToken, s.config.TokenKey, tokenTTL)
	if err != nil {
		return err
	}

	if payload != admin {
		return errors.New("token payload != admin")
	}
	return nil
}
