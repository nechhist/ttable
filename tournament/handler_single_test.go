package tournament

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/token"
)

// TestMongo ///////////////////////////////////////////
func Test_MongoServiceSingle(t *testing.T) {
	// 1
	HandleGetSingleCode(t, getTestServer(t))
	HandleGetSingleResponse(t, getTestServer(t))
	// 2
	HandleSingleCreateCode(t, getTestServer(t))
	HandleSingleCreateResponse(t, getTestServer(t))
	// 3
	HandleSingleUpdateCode(t, getTestServer(t))
	HandleSingleUpdateResponse(t, getTestServer(t))
	// 4
	HandleSingleUnitUpdateCode(t, getTestServer(t))
	HandleSingleUnitUpdateResponse(t, getTestServer(t))
	// 5
	HandleSingleUnitUpdateCode(t, getTestServer(t))
	HandleSingleUnitUpdateResponse(t, getTestServer(t))
}

// GetSingle ///////////////////////////////////////////
func HandleGetSingleCode(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name_get",
		Admin:       "admin_get",
		Description: "description_get",
		Table:       "table_get",
		ScoreEnable: 1,
	}
	if err := s.store.CreateSingle(testSingle); err != nil {
		t.Error(err)
	}

	testCases := []struct {
		name string
		id   string
		code int
	}{
		{
			name: "HandleGetSingleCode - normal",
			id:   testSingle.ID.Hex(),
			code: http.StatusOK,
		},
		{
			name: "HandleGetSingleCode - fail id",
			id:   "fail",
			code: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + tc.id
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, url, nil)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body: %v", url, string(htmlData))
		})
	}
}

func HandleGetSingleResponse(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "test_get",
		Admin:       "test_get",
		Description: "test_get",
		Table:       "table_get",
		ScoreEnable: 1,
	}
	if err := s.store.CreateSingle(testSingle); err != nil {
		t.Error(err)
	}

	testCases := []struct {
		name     string
		id       string
		response *model.Single
	}{
		{
			name:     "HandleGetSingleResponse - normal",
			id:       testSingle.ID.Hex(),
			response: testSingle,
		},
		{
			name:     "HandleGetSingleResponse - fail id",
			id:       "fail",
			response: &model.Single{},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + tc.id
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, url, nil)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			response := &model.Single{}
			json.NewDecoder(rec.Body).Decode(response)
			assert.Equalf(t, tc.response, response, "url: %v", url)
		})
	}
}

// SingleCreate ///////////////////////////////////////////
func HandleSingleCreateCode(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name_create",
		Admin:       "admin_create",
		Description: "dest_create",
		Table:       "table_create",
		ScoreEnable: 1,
	}
	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SingleRequest
		code    int
	}{
		{
			name: "HandleSingleCreateCode - normal",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        testSingle.Name,
				Table:       testSingle.Table,
				Description: testSingle.Description,
				ScoreEnable: testSingle.ScoreEnable,
			},
			code: http.StatusCreated,
		},
		{
			name:    "HandleSingleCreateCode - empty payload",
			payload: &SingleRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleSingleCreateCode - fail Token",
			payload: &SingleRequest{
				Token:       "fail",
				Admin:       testSingle.Admin,
				Name:        testSingle.Name,
				Table:       testSingle.Table,
				Description: testSingle.Description,
				ScoreEnable: testSingle.ScoreEnable,
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleSingleCreateCode - empty name",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        "",
				Table:       testSingle.Table,
				Description: testSingle.Description,
				ScoreEnable: testSingle.ScoreEnable,
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body: %v", url, string(htmlData))
		})
	}
}

func HandleSingleCreateResponse(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name_create",
		Admin:       "admin_create",
		Description: "dest_create",
		Table:       "Table_create",
		ScoreEnable: 1,
		Units:       make(map[string]model.Unit),
		Times:       make(map[string]model.Time),
		Scores:      make(map[string]model.Score),
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SingleRequest
		result  *model.Single
	}{
		{
			name: "HandleSingleCreateResponse - normal",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        testSingle.Name,
				Type:        testSingle.Type,
				Table:       testSingle.Table,
				ScoreEnable: testSingle.ScoreEnable,
				Description: testSingle.Description,
			},
			result: testSingle,
		},
		{
			name: "HandleSingleCreateResponse - trim name",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        " " + testSingle.Name + " ",
				Type:        testSingle.Type,
				Table:       " " + testSingle.Table + " ",
				Description: " " + testSingle.Description + " ",
				ScoreEnable: testSingle.ScoreEnable,
			},
			result: testSingle,
		},
		{
			name:    "HandleSingleCreateResponse - empty payload",
			payload: &SingleRequest{},
			result:  &model.Single{},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			single := &model.Single{}
			json.NewDecoder(rec.Body).Decode(single)
			tc.result.ID = single.ID
			tc.result.TimeUpdate = single.TimeUpdate
			tc.result.TimeCreate = single.TimeCreate

			assert.Equalf(t, tc.result, single, "url:%v", url)
		})
	}
}

// SingleUpdate ///////////////////////////////////////////
func HandleSingleUpdateCode(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name_update",
		Admin:       "admin_update",
		Description: "dest_update",
		Table:       "table_update",
		ScoreEnable: 0,
		Units:       make(map[string]model.Unit),
		Times:       make(map[string]model.Time),
		Scores:      make(map[string]model.Score),
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SingleRequest
		code    int
	}{
		{
			name: "HandleSingleUpdateCode - normal",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        "name update2",
				Table:       "table update2",
				Description: "description update2",
				ScoreEnable: 1,
			},
			code: http.StatusOK,
		},
		{
			name:    "HandleSingleUpdateCode - empty payload",
			payload: &SingleRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleSingleUpdateCode -fail Token",
			payload: &SingleRequest{
				Token: "fail",
				Admin: testSingle.Admin,
				Name:  testSingle.Name,
				Table: testSingle.Table,
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleSingleUpdateCode - empty name",
			payload: &SingleRequest{
				Token: _token,
				Admin: testSingle.Admin,
				Name:  "",
				Table: testSingle.Table,
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex()
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSingleUpdateResponse(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name_update",
		Admin:       "admin_update",
		Description: "dest_update",
		Table:       "table_update",
		ScoreEnable: 0,
		Units:       make(map[string]model.Unit),
		Times:       make(map[string]model.Time),
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SingleRequest
		single  model.Single
	}{
		{
			name: "HandleSingleUpdateResponse - normal",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        "name update2",
				Table:       "table update2",
				Description: "description update2",
				ScoreEnable: 1,
			},
			single: model.Single{
				Admin:       testSingle.Admin,
				Name:        "name update2",
				Table:       "table update2",
				Description: "description update2",
				ScoreEnable: 1,
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
		{
			name: "HandleSingleUpdateResponse - trim",
			payload: &SingleRequest{
				Token:       _token,
				Admin:       testSingle.Admin,
				Name:        " name update3 ",
				Table:       " table update3 ",
				Description: " description update3 ",
				ScoreEnable: 1,
			},
			single: model.Single{
				Admin:       testSingle.Admin,
				Name:        "name update3",
				Table:       "table update3",
				Description: "description update3",
				ScoreEnable: 1,
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex()
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			single := model.Single{}
			json.NewDecoder(rec.Body).Decode(&single)

			tc.single.ID = single.ID
			tc.single.TimeCreate = single.TimeCreate
			tc.single.TimeUpdate = single.TimeUpdate
			assert.Equalf(t, tc.single, single, "url:%v", url)
		})
	}
}

// SingleUnit ///////////////////////////////
func HandleSingleUnitUpdateCode(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:  "name",
		Admin: "admin",
		Table: "table",
		Units: map[string]model.Unit{},
		Times: map[string]model.Time{},
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *SingleUnitRequest
		code    int
	}{
		{
			name: "HandleSingleUnitUpdateCode - normal create",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "3_2_1_0",
				Value:  "value",
				Score:  1,
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSingleUnitUpdateCode - normal update",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "3_2_1_0",
				Value:  "value1",
				Score:  1,
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSingleUnitUpdateCode - normal update empty",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "3_2_1_0",
				Value:  "",
				Score:  1,
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSingleUnitUpdateCode - fail userID",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "123",
				Value:  "",
				Score:  0,
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex() + "/unit"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSingleUnitUpdateResponse(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Table:       "table",
		Units:       map[string]model.Unit{},
		Times:       map[string]model.Time{},
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SingleUnitRequest
		single  model.Single
	}{
		{
			name: "HandleSetUnitRequest - normal create",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "3_2_1_0",
				Value:  " value ",
				Score:  1,
			},
			single: model.Single{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Table:       "table",
				Units: map[string]model.Unit{
					"3_2_1_0": {
						Value: "value",
						Score: 1,
					},
				},
				Times: map[string]model.Time{},
			},
		},
		{
			name: "HandleSetUnitRequest - normal update",
			payload: &SingleUnitRequest{
				Token: _token,
				Admin: testSingle.Admin,

				UnitID: "3_2_1_0",
				Value:  "value update",
				Score:  1,
			},
			single: model.Single{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Table:       "table",
				Units: map[string]model.Unit{
					"3_2_1_0": {
						Value: "value update",
						Score: 1,
					},
				},
				Times: map[string]model.Time{},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex() + "/unit"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			single := model.Single{}
			json.NewDecoder(rec.Body).Decode(&single)

			tc.single.ID = single.ID
			tc.single.TimeCreate = single.TimeCreate
			tc.single.TimeUpdate = single.TimeUpdate
			assert.Equalf(t, tc.single, single, "url:%v", url)
		})
	}
}

// SingleUnit ///////////////////////////////
func HandleSingleTimeUpdateCode(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Table:       "table",
		Units:       map[string]model.Unit{},
		Times:       map[string]model.Time{},
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *SingleTimeRequest
		code    int
	}{
		{
			name: "HandleSetTimeCode - normal create",
			id:   "1",
			payload: &SingleTimeRequest{
				Token:  _token,
				Admin:  testSingle.Admin,
				TimeID: "3_2_1_0",
				Value:  "value",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetTimeCode - normal update empty",
			id:   "1",
			payload: &SingleTimeRequest{
				Token:  _token,
				Admin:  testSingle.Admin,
				TimeID: "3_2_1_0",
				Value:  "",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetTimeCode- fail timeID",
			id:   "1",
			payload: &SingleTimeRequest{
				Token:  _token,
				Admin:  testSingle.Admin,
				TimeID: "123",
				Value:  "",
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex() + "/time"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSingleTimeUpdateRequest(t *testing.T, s Service) {
	testSingle := &model.Single{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Table:       "table",
		Units:       map[string]model.Unit{},
		Times:       map[string]model.Time{},
	}

	testSingle.Format()
	testSingle.BeforeCreate()
	err := s.store.CreateSingle(testSingle)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testSingle.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *SingleTimeRequest
		single  model.Single
	}{
		{
			name: "HandleSeTimeRequest - normal create",
			payload: &SingleTimeRequest{
				Token:  _token,
				Admin:  testSingle.Admin,
				TimeID: "3_2_1_0",
				Value:  " value ",
			},
			single: model.Single{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Table:       "table",
				Times: map[string]model.Time{
					"3_2_1_0": {
						Value: "value",
					},
				},
			},
		},
		{
			name: "HandleSeTimeRequest - normal update",
			payload: &SingleTimeRequest{
				Token: _token,
				Admin: testSingle.Admin,

				TimeID: "3_2_1_0",
				Value:  "",
			},
			single: model.Single{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Table:       "table",
				Times: map[string]model.Time{
					"3_2_1_0": {
						Value: "",
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/single/" + testSingle.ID.Hex() + "/time"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			single := model.Single{}
			json.NewDecoder(rec.Body).Decode(&single)

			tc.single.ID = single.ID
			tc.single.TimeCreate = single.TimeCreate
			tc.single.TimeUpdate = single.TimeUpdate
			assert.Equalf(t, tc.single, single, "url:%v", url)
		})
	}
}
