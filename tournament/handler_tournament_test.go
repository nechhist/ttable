package tournament

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/token"
)

// TestMongo ///////////////////////////////////////////
func Test_MongoServiceTournament(t *testing.T) {
	// 1
	HandleGetTournamentCode(t, getTestServer(t))
	HandleGetTournamentResponse(t, getTestServer(t))
	// 2
	HandleTournamentCreateCode(t, getTestServer(t))
	HandleTournamentCreateResponse(t, getTestServer(t))
	// 3
	HandleTournamentUpdateCode(t, getTestServer(t))
	HandleTournamentUpdateResponse(t, getTestServer(t))
	// 4
	HandleSeasonCreateCode(t, getTestServer(t))
	HandleSeasonCreateResponse(t, getTestServer(t))
	// 5
	HandleSeasonUpdateCode(t, getTestServer(t))
	HandleSeasonUpdateResponse(t, getTestServer(t))
	// 6
	HandleSetUnitCode(t, getTestServer(t))
	HandleSetUnitRequest(t, getTestServer(t))
	// 7
	HandleSetTimeCode(t, getTestServer(t))
	HandleSeTimeRequest(t, getTestServer(t))
	// 8
	// HandleMyTournamentsCodeResponse(t, getTestServer(t))
	// 9
	// HandleIndexCodeResponse(t, getTestServer(t))
}

// GetTournament ///////////////////////////////////////////

func HandleGetTournamentCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:  "Name",
		Admin: "Admin",
	}
	if err := s.store.CreateTournament(testTournament); err != nil {
		t.Error(err)
	}

	testCases := []struct {
		name string
		id   string
		code int
	}{
		{
			name: "HandleGetTournamentCode - normal",
			id:   strconv.Itoa(int(testTournament.ID)),
			code: http.StatusOK,
		},
		{
			name: "HandleGetTournamentCode - fail id",
			id:   "999",
			code: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + tc.id
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, url, nil)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleGetTournamentResponse(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:  "Name",
		Admin: "Admin",
	}
	if err := s.store.CreateTournament(testTournament); err != nil {
		t.Error(err)
	}

	testCases := []struct {
		name     string
		id       string
		response *model.Tournament
	}{
		{
			name:     "HandleGetTournamentResponse - normal",
			id:       strconv.Itoa(int(testTournament.ID)),
			response: testTournament,
		},
		{
			name:     "HandleGetTournamentResponse - fail id",
			id:       "999",
			response: &model.Tournament{},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + tc.id
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, url, nil)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			response := &model.Tournament{}
			if err := json.NewDecoder(rec.Body).Decode(&response); err != nil {
				t.Error(err)
			}
			assert.Equalf(t, tc.response, response, "url: %v", url)
		})
	}
}

// TournamentCreate ///////////////////////////////////////////
func HandleTournamentCreateCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name_create",
		Admin:       "admin_create",
		Description: "dest_create",
		Game:        "game_create",
		Logo:        "logo_create",
	}
	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *TournamentRequest
		code    int
	}{
		{
			name: "HandleTournamentCreateCode - normal",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			code: http.StatusCreated,
		},
		{
			name:    "HandleTournamentCreateCode - empty payload",
			payload: &TournamentRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleTournamentCreateCode - fail Token",
			payload: &TournamentRequest{
				Token:       "fail",
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleTournamentCreateCode - empty name",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "",
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleTournamentCreateResponse(t *testing.T, s Service) {
	testTournament := model.Tournament{
		Name:        "name_create",
		Admin:       "admin_create",
		Description: "dest_create",
		Game:        "game_create",
		Logo:        "logo_create",
		Seasons:     make(map[uint64]model.Season),
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		payload    *TournamentRequest
		tournament model.Tournament
	}{
		{
			name: "HandleTournamentCreateResponse - normal",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			tournament: testTournament,
		},
		{
			name: "HandleTournamentCreateResponse - trim name",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        " " + testTournament.Name + " ",
				Game:        " " + testTournament.Game + " ",
				Description: " " + testTournament.Description + " ",
				Logo:        " " + testTournament.Logo + " ",
			},
			tournament: func() model.Tournament {
				t := testTournament
				t.ID = t.ID + 1
				return t
			}(),
		},
		{
			name:       "HandleTournamentCreateResponse - empty payload",
			payload:    &TournamentRequest{},
			tournament: model.Tournament{},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			json.NewDecoder(rec.Body).Decode(&tournament)
			tc.tournament.ID = tournament.ID
			assert.Equal(t, tc.tournament, tournament, "url:%v", url)
		})
	}
}

// TournamentUpdate ///////////////////////////////////////////
func HandleTournamentUpdateCode(t *testing.T, s Service) {
	testTournament := model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Logo:        "logo",
		Seasons:     make(map[uint64]model.Season),
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(&testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *TournamentRequest
		code    int
	}{
		{
			name: "HandleTournamentUpdateCode - normal",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "test_update",
				Game:        "game_update",
				Description: "description_update",
				Logo:        "logo_update",
			},
			code: http.StatusOK,
		},
		{
			name:    "HandleTournamentUpdateCode - empty payload",
			payload: &TournamentRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleTournamentUpdateCode -fail Token",
			payload: &TournamentRequest{
				Token:       "fail",
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleTournamentUpdateCode - empty name",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "",
				Game:        testTournament.Game,
				Description: testTournament.Description,
				Logo:        testTournament.Logo,
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID))
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleTournamentUpdateResponse(t *testing.T, s Service) {
	testTournament := model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Logo:        "logo",
		Seasons:     make(map[uint64]model.Season),
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(&testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		payload    *TournamentRequest
		tournament model.Tournament
	}{
		{
			name: "HandleTournamentUpdateResponse - normal",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "name update",
				Game:        "game update",
				Description: "description update",
				Logo:        "logo_update",
			},
			tournament: model.Tournament{
				Admin:       testTournament.Admin,
				Name:        "name update",
				Description: "description update",
				Game:        "game update",
				Logo:        "logo_update",
				Seasons:     testTournament.Seasons,
			},
		},
		{
			name: "HandleTournamentUpdateResponse - trim",
			payload: &TournamentRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        " name update2 ",
				Game:        " game update2 ",
				Description: " description update2 ",
				Logo:        " logo_update2 ",
			},
			tournament: model.Tournament{
				Admin:       testTournament.Admin,
				Name:        "name update2",
				Description: "description update2",
				Game:        "game update2",
				Logo:        "logo_update2",
				Seasons:     testTournament.Seasons,
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID))
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			json.NewDecoder(rec.Body).Decode(&tournament)
			tc.tournament.ID = tournament.ID
			assert.Equalf(t, tc.tournament, tournament, "url:", url)
		})
	}
}

// SeasonCreate ///////////////////////////////////////////
func HandleSeasonCreateCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons:     make(map[uint64]model.Season),
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload *SeasonRequest
		code    int
	}{
		{
			name: "HandleSeasonCreateCode - normal",
			payload: &SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				ScoreEnable: 1,
			},
			code: http.StatusCreated,
		},
		{
			name:    "HandleSeasonCreateCode - empty payload",
			payload: &SeasonRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleSeasonCreateCode - fail Token",
			payload: &SeasonRequest{
				Token:      "fail token",
				Admin:      testTournament.Admin,
				Name:       "name",
				TimeAction: "timeAction",
				Table:      "table",
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleSeasonCreateCode - empty name",
			payload: &SeasonRequest{
				Token:      _token,
				Admin:      testTournament.Admin,
				Name:       "",
				TimeAction: "timeAction",
				Table:      "table",
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "recorder body:%v", string(htmlData))
		})
	}
}

func HandleSeasonCreateResponse(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		ID:          1,
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons:     make(map[uint64]model.Season),
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		payload    SeasonRequest
		tournament model.Tournament
	}{
		{
			name: "HandleSeasonCreateResponse - normal",
			payload: SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				ScoreEnable: 1,
			},
			tournament: model.Tournament{
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Description: testTournament.Description,
				Game:        testTournament.Game,
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 1,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units:       make(map[string]model.Unit),
						Times:       make(map[string]model.Time),
						Scores:      make(map[string]model.Score),
					},
				},
			},
		},
		{
			name: "HandleSeasonCreateResponse - add second + trim",
			payload: SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        " name2 ",
				TimeAction:  " timeAction2 ",
				Table:       " table2 ",
				ScoreEnable: 1,
			},
			tournament: model.Tournament{
				ID:          1,
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Description: testTournament.Description,
				Game:        testTournament.Game,
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 1,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units:       make(map[string]model.Unit),
						Times:       make(map[string]model.Time),
						Scores:      make(map[string]model.Score),
					},
					2: {
						Name:        "name2",
						TimeAction:  "timeAction2",
						Table:       "table2",
						ScoreEnable: 1,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units:       make(map[string]model.Unit),
						Times:       make(map[string]model.Time),
						Scores:      make(map[string]model.Score),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			if err := json.NewDecoder(rec.Body).Decode(&tournament); err != nil {
				t.Error(err)
			}
			tournament.ID = tc.tournament.ID
			assert.Equalf(t, tc.tournament, tournament, "url:%v", url)
		})
	}
}

// SeasonCreate ///////////////////////////////////////////
func HandleSeasonUpdateCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name0",
				TimeAction:  "timeAction0",
				Table:       "table0",
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
				Scores:      make(map[string]model.Score),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *SeasonRequest
		code    int
	}{
		{
			name: "HandleSeasonUpdateCode - normal",
			id:   "1",
			payload: &SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "name1",
				TimeAction:  "timeAction1",
				Table:       "table1",
				ScoreEnable: 1,
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSeasonUpdateCode - fail sid",
			id:   "3",
			payload: &SeasonRequest{
				Token:      _token,
				Admin:      testTournament.Admin,
				Name:       "name1",
				TimeAction: "timeAction1",
				Table:      "table1",
			},
			code: http.StatusNotFound,
		},
		{
			name:    "HandleSeasonUpdateCode - empty payload",
			id:      "1",
			payload: &SeasonRequest{},
			code:    http.StatusBadRequest,
		},
		{
			name: "HandleSeasonUpdateCode - fail Token",
			id:   "1",
			payload: &SeasonRequest{
				Token:      "fail token",
				Admin:      testTournament.Admin,
				Name:       "name",
				TimeAction: "timeAction",
				Table:      "table",
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "HandleSeasonUpdateCode - empty name",
			id:   "1",
			payload: &SeasonRequest{
				Token:      _token,
				Admin:      testTournament.Admin,
				Name:       "",
				TimeAction: "timeAction",
				Table:      "table",
			},
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/" + tc.id
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSeasonUpdateResponse(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name0",
				TimeAction:  "timeAction0",
				Table:       "table0",
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
				Scores:      make(map[string]model.Score),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		payload    SeasonRequest
		tournament model.Tournament
	}{
		{
			name: "HandleSeasonUpdateResponse - normal",
			payload: SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        "name1",
				TimeAction:  "timeAction1",
				Table:       "table1",
				ScoreEnable: 1,
			},
			tournament: model.Tournament{
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Description: testTournament.Description,
				Game:        testTournament.Game,
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name1",
						TimeAction:  "timeAction1",
						Table:       "table1",
						ScoreEnable: 1,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units:       make(map[string]model.Unit),
						Times:       make(map[string]model.Time),
						Scores:      make(map[string]model.Score),
					},
				},
			},
		},

		{
			name: "HandleSeasonUpdateResponse - add second + trim",
			payload: SeasonRequest{
				Token:       _token,
				Admin:       testTournament.Admin,
				Name:        " name2 ",
				TimeAction:  " timeAction2 ",
				Table:       " table2 ",
				ScoreEnable: 1,
			},
			tournament: model.Tournament{
				ID:          1,
				Admin:       testTournament.Admin,
				Name:        testTournament.Name,
				Description: testTournament.Description,
				Game:        testTournament.Game,
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name2",
						TimeAction:  "timeAction2",
						Table:       "table2",
						ScoreEnable: 1,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units:       make(map[string]model.Unit),
						Times:       make(map[string]model.Time),
						Scores:      make(map[string]model.Score),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/1"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			json.NewDecoder(rec.Body).Decode(&tournament)
			tournament.ID = tc.tournament.ID
			assert.Equalf(t, tc.tournament, tournament, "url:%v", url)
		})
	}
}

// SetUnit ///////////////////////////////
func HandleSetUnitCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *TournamentUnitRequest
		code    int
	}{
		{
			name: "HandleSetUnitCode - normal",
			id:   "1",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "3_2_1_0",
				Value:  "value",
				Score:  1,
				Logo:   "logo",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetUnitCode - normal empty",
			id:   "1",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "3_2_1_0",
				Value:  "",
				Score:  1,
				Logo:   "",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetUnitCode - normal empty",
			id:   "1",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "3_2_1_0",
				Value:  "value2",
				Score:  1,
				Logo:   "",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetUnitCode - fail userID",
			id:   "1",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "123",
				Value:  "",
				Score:  0,
				Logo:   "",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "HandleSetUnitCode - fail seasonID",
			id:   "3",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "1_1_0_0",
				Value:  "",
				Score:  0,
				Logo:   "",
			},
			code: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/" + tc.id + "/unit"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)

			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSetUnitRequest(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		payload    *TournamentUnitRequest
		tournament model.Tournament
	}{
		{
			name: "HandleSetUnitRequest - normal",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "3_2_1_0",
				Value:  " value ",
				Score:  1,
				//Logo:   "logo",
			},
			tournament: model.Tournament{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Game:        "game",
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 0,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units: map[string]model.Unit{
							"3_2_1_0": {
								Value: "value",
								Score: 1,
								// Logo:       "logo",
							},
						},
						Times: make(map[string]model.Time),
					},
				},
			},
		},
		{
			name: "HandleSetUnitRequest - normal",
			payload: &TournamentUnitRequest{
				Token: _token,
				Admin: testTournament.Admin,

				UnitID: "3_2_1_0",
				Value:  "value1",
				Score:  1,
				//Logo:   "logo",
			},
			tournament: model.Tournament{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Game:        "game",
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 0,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Units: map[string]model.Unit{
							"3_2_1_0": {
								Value: "value1",
								Score: 1,
								// Logo:       "logo",
							},
						},
						Times: make(map[string]model.Time),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/1/unit"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			json.NewDecoder(rec.Body).Decode(&tournament)
			tc.tournament.ID = tournament.ID
			tc.tournament.TimeCreate = tournament.TimeCreate
			tc.tournament.TimeUpdate = tournament.TimeUpdate

			assert.Equalf(t, tc.tournament, tournament, "%v", url)
		})
	}
}

// SetUnit ///////////////////////////////
func HandleSetTimeCode(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				Status:      0,
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		id      string
		payload *TournamentTimeRequest
		code    int
	}{
		{
			name: "HandleSetTimeCode - normal",
			id:   "1",
			payload: &TournamentTimeRequest{
				Token:  _token,
				Admin:  testTournament.Admin,
				TimeID: "3_2_1_0",
				Value:  "value",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetTimeCode - normal retry",
			id:   "1",
			payload: &TournamentTimeRequest{
				Token:  _token,
				Admin:  testTournament.Admin,
				TimeID: "3_2_1_0",
				Value:  "",
			},
			code: http.StatusOK,
		},
		{
			name: "HandleSetTimeCode- fail timeID",
			id:   "1",
			payload: &TournamentTimeRequest{
				Token:  _token,
				Admin:  testTournament.Admin,
				TimeID: "123",
				Value:  "",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "HandleSetTimeCode- fail sid",
			id:   "3",
			payload: &TournamentTimeRequest{
				Token:  _token,
				Admin:  testTournament.Admin,
				TimeID: "1_1_0_0",
				Value:  "",
			},
			code: http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/" + tc.id + "/time"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url:%v, recorder body:%v", url, string(htmlData))
		})
	}
}

func HandleSeTimeRequest(t *testing.T, s Service) {
	testTournament := model.Tournament{
		Name:        "name",
		Admin:       "admin",
		Description: "description",
		Game:        "game",
		Seasons: map[uint64]model.Season{
			1: {
				Name:        "name",
				TimeAction:  "timeAction",
				Table:       "table",
				ScoreEnable: 0,
				TimeCreate:  time.Now().Unix(),
				TimeUpdate:  time.Now().Unix(),
				Units:       make(map[string]model.Unit),
				Times:       make(map[string]model.Time),
			},
		},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(&testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name       string
		id         string
		payload    *TournamentTimeRequest
		tournament model.Tournament
	}{
		{
			name: "HandleSeTimeRequest - normal",
			id:   "1",
			payload: &TournamentTimeRequest{
				Token:  _token,
				Admin:  testTournament.Admin,
				TimeID: "3_2_1_0",
				Value:  " value ",
			},
			tournament: model.Tournament{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Game:        "game",
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 0,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Times: map[string]model.Time{
							"3_2_1_0": {
								Value: "value",
							},
						},
						Units: make(map[string]model.Unit),
					},
				},
			},
		},
		{
			name: "HandleSeTimeRequest - normal - change this id",
			id:   "1",
			payload: &TournamentTimeRequest{
				Token: _token,
				Admin: testTournament.Admin,

				TimeID: "3_2_1_0",
				Value:  "value1",
			},
			tournament: model.Tournament{
				Name:        "name",
				Admin:       "admin",
				Description: "description",
				Game:        "game",
				Seasons: map[uint64]model.Season{
					1: {
						Name:        "name",
						TimeAction:  "timeAction",
						Table:       "table",
						ScoreEnable: 0,
						TimeCreate:  time.Now().Unix(),
						TimeUpdate:  time.Now().Unix(),
						Times: map[string]model.Time{
							"3_2_1_0": {
								Value: "value1",
							},
						},
						Units: make(map[string]model.Unit),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/" + strconv.Itoa(int(testTournament.ID)) + "/season/" + tc.id + "/time"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			tournament := model.Tournament{}
			json.NewDecoder(rec.Body).Decode(&tournament)
			tc.tournament.ID = tournament.ID
			assert.Equalf(t, tc.tournament, tournament, "url:%v", url)
		})
	}
}

// MyTournaments ///////////////////////////////
func HandleMyTournamentsCodeResponse(t *testing.T, s Service) {
	testTournament := &model.Tournament{
		Name:        "name",
		Admin:       strconv.Itoa(int(time.Now().UnixNano())),
		Description: "description",
		Game:        "game",
		Seasons:     map[uint64]model.Season{},
	}

	testTournament.Format()
	testTournament.BeforeCreate()
	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		request MyRequest
		code    int
		result  string
	}{
		{
			name: "HandleMyTournamentsCodeResponse - normal",
			request: MyRequest{
				Token: _token,
				Admin: testTournament.Admin,
			},
			code:   http.StatusOK,
			result: "{\"" + strconv.Itoa(int(testTournament.ID)) + "\":\"" + testTournament.Name + "\"}\n",
		},
	}

	for _, tc := range testCases {
		url := "/api/v1/tournament/my"
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.request)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "recorder body:%v", string(htmlData))
			assert.Equal(t, tc.result, string(htmlData))
		})
	}
}

// Index //////////////////////////////
func HandleIndexCodeResponse(t *testing.T, s Service) {
	// add tournament
	testTournament := &model.Tournament{
		Admin: "admin",
		Name:  "name",
	}

	err := s.store.CreateTournament(testTournament)
	if err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testTournament.Admin, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	// add season
	seasonRequest := &SeasonRequest{
		Token: _token,
		Admin: testTournament.Admin,
		Name:  "s_name",
		Table: "table",
	}
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(seasonRequest)
	rec := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/api/v1/tournament/"+strconv.Itoa(int(testTournament.ID))+"/season", b)
	s.httpServer.Handler.ServeHTTP(rec, req)
	htmlData, _ := ioutil.ReadAll(rec.Body)
	assert.Equalf(t, 201, rec.Code, "html: %v", string(htmlData))

	// check index
	rec = httptest.NewRecorder()
	req, _ = http.NewRequest(http.MethodGet, "/api/v1/index", b)
	s.httpServer.Handler.ServeHTTP(rec, req)

	htmlData, _ = ioutil.ReadAll(rec.Body)
	assert.Equal(t, 200, rec.Code)
	now := strconv.Itoa(int(time.Now().Unix()))
	assert.Equal(t, "[{\"tournament_id\":"+strconv.Itoa(int(testTournament.ID))+",\"tournament_name\":\"name\",\"season_name\":\"s_name\",\"season_id\":1,\"admin\":\"admin\",\"time\":"+now+"}]\n", string(htmlData))

	// add season with new name
	seasonRequest = &SeasonRequest{
		Token: _token,
		Admin: testTournament.Admin,
		Name:  "s_name_new",
		Table: "table",
	}
	b = &bytes.Buffer{}
	json.NewEncoder(b).Encode(seasonRequest)
	rec = httptest.NewRecorder()
	req, _ = http.NewRequest(http.MethodPost, "/api/v1/tournament/"+strconv.Itoa(int(testTournament.ID))+"/season/1", b)
	s.httpServer.Handler.ServeHTTP(rec, req)
	htmlData, _ = ioutil.ReadAll(rec.Body)
	assert.Equalf(t, 200, rec.Code, "html: %v", string(htmlData))

	// check index
	rec = httptest.NewRecorder()
	req, _ = http.NewRequest(http.MethodGet, "/api/v1/index", b)
	s.httpServer.Handler.ServeHTTP(rec, req)

	htmlData, _ = ioutil.ReadAll(rec.Body)
	assert.Equal(t, 200, rec.Code)
	now = strconv.Itoa(int(time.Now().Unix()))
	assert.Equal(t, "[{\"tournament_id\":"+strconv.Itoa(int(testTournament.ID))+",\"tournament_name\":\"name\",\"season_name\":\"s_name_new\",\"season_id\":1,\"admin\":\"admin\",\"time\":"+now+"}]\n", string(htmlData))
}
