package tournament

import (
	"encoding/json"
	"net/http"

	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/antispam"
)

func (s *Service) handleSingleGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableStr(r, "id")
		if err != nil {
			s.logger.Info("handleSingleGet getVariableInt:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleGet/getVariableInt")
			return
		}

		Single, err := s.store.FindSingleByID(id)
		if err != nil {
			s.logger.Info("handleSingleGet findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleSingleGet/findByID")
			return
		}
		s.response(w, r, http.StatusOK, Single, "handleSingleGet")
	}
}

// SingleRequest ...
type SingleRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	Name        string `json:"name"`
	Description string `json:"description"`
	Table       string `json:"table"`
	ScoreEnable uint64 `json:"score_enable"`
	Type        uint64 `json:"-"`
}

func (s *Service) handleSingleCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// antispam
		if err := s.spamStore.Add(r.RemoteAddr); err != nil {
			if err == antispam.ErrIPExists {
				s.logger.Info("handleSingleCreate anti_spam add ErrIPExists error:", http.StatusUnprocessableEntity, errResponseSpam)
				s.response(w, r, http.StatusUnprocessableEntity, errResponseSpam, "handleSingleCreate/ErrIPExists")
			} else {
				s.logger.Info("handleSingleCreate anti_spam add error:", http.StatusUnprocessableEntity, err)
				s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleCreate/spamStore.Add")
			}
			return
		}

		// get json
		var singleRequest SingleRequest
		if err := json.NewDecoder(r.Body).Decode(&singleRequest); err != nil {
			s.logger.Info("handleSingleCreate json Decode:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleCreate/json.Decode")
			return
		}

		if singleRequest.Admin == "" || singleRequest.Token == "" {
			s.logger.Error("handleSingleCreate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSingleCreate/empty")
			return
		}

		// check secret
		if err := s.checkSecret(singleRequest.Admin, singleRequest.Token); err != nil {
			s.logger.Error("handleSingleCreate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSingleCreate/checkSecret")
			return
		}

		single := &model.Single{}
		single.Name = singleRequest.Name
		single.Admin = singleRequest.Admin
		single.Table = singleRequest.Table
		single.Description = singleRequest.Description
		single.ScoreEnable = singleRequest.ScoreEnable

		single.Units = make(map[string]model.Unit)
		single.Times = make(map[string]model.Time)
		single.Scores = make(map[string]model.Score)

		single.Format()
		single.BeforeCreate()
		if err := single.Validate(); err != nil {
			s.logger.Error("handleSingleCreate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleCreate/BeforeCreate")
			return
		}

		if err := s.store.CreateSingle(single); err != nil {
			s.logger.Error("handleSingleCreate createSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleCreate/createSingle")
			return
		}
		if err := s.indexStore.add(
			TypeIndexSingle, single.ID.Hex(),
			single.Name,
			single.Admin,
			single.TimeCreate,
		); err != nil {
			s.logger.Error("handleSingleCreate indexStore.add:", err)
		}

		go s.telegram.SendMessage("Create Single " + single.Name + "(id: " + single.ID.Hex() + ") admin: " + single.Admin)
		s.response(w, r, http.StatusCreated, single, "handleSingleCreate")
	}
}

func (s *Service) handleSingleUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var singleRequest SingleRequest
		if err := json.NewDecoder(r.Body).Decode(&singleRequest); err != nil {
			s.logger.Error("handleSingleUpdate json Decode:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUpdate/json.Decode")
			return
		}

		if singleRequest.Admin == "" || singleRequest.Token == "" {
			s.logger.Error("handleSingleUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSingleUpdate/empty")
			return
		}

		id, err := getVariableStr(r, "id")
		if err != nil {
			s.logger.Info("handleSingleUpdate getVariableInt:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(singleRequest.Admin, singleRequest.Token); err != nil {
			s.logger.Info("handleSingleUpdate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSingleUpdate/checkSecret")
			return
		}

		single, err := s.store.FindSingleByID(id)
		if err != nil {
			s.logger.Info("handleSingleUpdate findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleSingleUpdate/findByID")
			return
		}

		if single.Admin != singleRequest.Admin {
			s.logger.Info("handleSingleUpdate: admin single not equal", http.StatusBadRequest)
			s.response(w, r, http.StatusBadRequest, "admin single not equal", "handleSingleUpdate/admin")
			return
		}

		single.Name = singleRequest.Name
		single.Admin = singleRequest.Admin
		single.Table = singleRequest.Table
		single.Description = singleRequest.Description
		single.ScoreEnable = singleRequest.ScoreEnable

		single.Format()
		single.BeforeUpdate()
		if err := single.Validate(); err != nil {
			s.logger.Error("handleSingleUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUpdate/Validate")
			return
		}

		if err := s.store.UpdateSingle(single); err != nil {
			s.logger.Error("handleSingleUpdate updateSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleUpdate/updateSingle")
			return
		}

		s.postSingleToWS(single)
		s.response(w, r, http.StatusOK, single, "handleSingleUpdate/updateSingle")
	}
}

// SingleUnitRequest ...
type SingleUnitRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	UnitID string `json:"unit_id"`
	Value  string `json:"value"`
	Score  uint64 `json:"score"`
	Logo   string `json:"-"`
}

func (s *Service) handleSingleUnitUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var unitRequest SingleUnitRequest
		if err := json.NewDecoder(r.Body).Decode(&unitRequest); err != nil {
			s.logger.Error("handleSingleUnitUpdate json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUnitUpdate/json.Decode")
			return
		}

		if unitRequest.Admin == "" || unitRequest.Token == "" {
			s.logger.Error("handleSingleUnitUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSingleUnitUpdate/empty")
			return
		}

		tid, err := getVariableStr(r, "id")
		if err != nil {
			s.logger.Error("handleSingleUnitUpdate getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUnitUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(unitRequest.Admin, unitRequest.Token); err != nil {
			s.logger.Error("handleSingleUnitUpdate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSingleUnitUpdate/checkSecret")
			return
		}

		single, err := s.store.FindSingleByID(tid)
		if err != nil {
			s.logger.Error("handleSingleUnitUpdate findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleSingleUnitUpdate/findByID")
			return
		}

		if single.Admin != unitRequest.Admin {
			s.logger.Info("handleSingleUnitUpdate:", http.StatusBadRequest, "admin single not equal")
			s.response(w, r, http.StatusBadRequest, "admin single not equal", "handleSingleUnitUpdate/admin")
			return
		}

		unit := model.Unit{}
		_, _, _, _, err = getUnitParams(unitRequest.UnitID)
		if err != nil {
			s.logger.Error("handleSingleUnitUpdate getUnitParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUnitUpdate/getUnitParams")
			return
		}
		unit.Value = unitRequest.Value
		unit.Score = unitRequest.Score

		unit = unit.Format()
		unit = unit.BeforeUpdate()
		if err = unit.Validate(); err != nil {
			s.logger.Error("handleSingleUnitUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleUnitUpdate/Validate")
			return
		}

		single.Units[unitRequest.UnitID] = unit
		single.BeforeUpdate()
		if err := s.store.UpdateSingle(single); err != nil {
			s.logger.Error("handleSingleUnitUpdate updateSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleUnitUpdate/updateSingle")
			return
		}

		s.postSingleToWS(single)
		s.response(w, r, http.StatusOK, single, "handleSingleUnitUpdate")
	}
}

// SingleTimeRequest ...
type SingleTimeRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	TimeID string `json:"time_id"`
	Value  string `json:"value"`
}

func (s *Service) handleSingleTimeUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var timeRequest SingleTimeRequest
		if err := json.NewDecoder(r.Body).Decode(&timeRequest); err != nil {
			s.logger.Error("handleSingleTimeUpdate json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleTimeUpdate/json.Decode")
			return
		}

		if timeRequest.Admin == "" || timeRequest.Token == "" {
			s.logger.Error("handleSingleTimeUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSingleTimeUpdate/admin")
			return
		}

		id, err := getVariableStr(r, "id")
		if err != nil {
			s.logger.Error("handleSingleTimeUpdate getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleTimeUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(timeRequest.Admin, timeRequest.Token); err != nil {
			s.logger.Error("handleSingleTimeUpdate checkSecret:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSingleTimeUpdate/checkSecret")
			return
		}

		single, err := s.store.FindSingleByID(id)
		if err != nil {
			s.logger.Error("handleSingleTimeUpdate findByID:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusNotFound, err, "handleSingleTimeUpdate/findByID")
			return
		}

		if single.Admin != timeRequest.Admin {
			s.logger.Info("handleSingleTimeUpdate:", http.StatusBadRequest, "admin single not equal")
			s.response(w, r, http.StatusBadRequest, "admin single not equal", "handleSingleTimeUpdate/noAdmin")
			return
		}

		_time := model.Time{}
		_, _, _, _, err = getUnitParams(timeRequest.TimeID)
		if err != nil {
			s.logger.Error("handleSingleTimeUpdate getUnitParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleTimeUpdate/getUnitParams")
			return
		}
		_time.Value = timeRequest.Value

		_time = _time.Format()
		_time = _time.BeforeUpdate()
		if err = _time.Validate(); err != nil {
			s.logger.Error("handleSingleTimeUpdate Validate:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleTimeUpdate/Validate")
			return
		}

		single.Times[timeRequest.TimeID] = _time
		single.BeforeUpdate()
		if err := s.store.UpdateSingle(single); err != nil {
			s.logger.Error("handleSingleTimeUpdate updateSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleTimeUpdate/updateSingle")
			return
		}

		s.postSingleToWS(single)
		s.response(w, r, http.StatusOK, single, "handleSingleTimeUpdate")
	}
}

// SingleScoreRequest ...
type SingleScoreRequest struct {
	Token string `json:"token"`
	Admin string `json:"admin"`

	ScoreID string `json:"score_id"`
	Unit1   string `json:"unit1"`
	Score1  uint64 `json:"score1"`
	Unit2   string `json:"unit2"`
	Score2  uint64 `json:"score2"`
}

func (s *Service) handleSingleScoreUpdate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get json
		var scoreRequest SingleScoreRequest
		if err := json.NewDecoder(r.Body).Decode(&scoreRequest); err != nil {
			s.logger.Error("handleSingleScoreUpdate json Decoder:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleScoreUpdate/json.Decode")
			return
		}

		if scoreRequest.Admin == "" || scoreRequest.Token == "" {
			s.logger.Error("handleSingleScoreUpdate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleSingleScoreUpdate/empty")
			return
		}

		sid, err := getVariableStr(r, "id")
		if err != nil {
			s.logger.Error("handleSingleScoreUpdate getVariableInt tid:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleScoreUpdate/getVariableInt")
			return
		}

		// check secret
		if err := s.checkSecret(scoreRequest.Admin, scoreRequest.Token); err != nil {
			s.logger.Error("handleSingleScoreUpdate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleSingleScoreUpdate/checkSecret")
			return
		}

		single, err := s.store.FindSingleByID(sid)
		if err != nil {
			s.logger.Error("handleSingleScoreUpdate findByID:", http.StatusNotFound, err)
			s.response(w, r, http.StatusNotFound, err, "handleSingleScoreUpdate/findByID")
			return
		}

		if single.Admin != scoreRequest.Admin {
			s.logger.Info("handleSingleScoreUpdate:", http.StatusBadRequest, "admin single not equal")
			s.response(w, r, http.StatusBadRequest, "admin single not equal", "handleSingleScoreUpdate/admin")
			return
		}

		//unit1, unit2, _type, up, err := getUnitParams(scoreRequest.ScoreID)
		_, _, _, _, err = getUnitParams(scoreRequest.ScoreID)
		if err != nil {
			s.logger.Error("handleSingleScoreUpdate getScoreParams:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleScoreUpdate/getScoreParams")
			return
		}

		score := model.Score{}
		score.Score1 = scoreRequest.Score1
		score.Score2 = scoreRequest.Score2

		score = score.Format()
		score = score.BeforeUpdate()
		if err = score.Validate(); err != nil {
			s.logger.Error("handleSingleScoreUpdate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleSingleScoreUpdate/Validate")
			return
		}

		single.Scores[scoreRequest.ScoreID] = score

		single.BeforeUpdate()
		if err := s.store.UpdateSingle(single); err != nil {
			s.logger.Error("handleSingleScoreUpdate updateSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleSingleScoreUpdate/updateSingle")
			return
		}

		s.postSingleToWS(single)
		s.response(w, r, http.StatusOK, single, "handleSingleScoreUpdate")
	}
}
