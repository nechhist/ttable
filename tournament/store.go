package tournament

import (
	"gitlab.com/nechhist/ttable/model"
)

// Store ...
type Store interface {
	InitMigration() error
	// tournament
	CreateTournament(tournament *model.Tournament) error
	UpdateTournament(tournament *model.Tournament) error
	FindTournamentByID(id uint64) (*model.Tournament, error)
	FindAllTournaments(offset, limit int64, field string, desc bool) ([]*model.Tournament, error)
	FindAllTournamentsByAdmin(admin string) (map[uint64]string, error)
	// Single
	CreateSingle(single *model.Single) error
	UpdateSingle(single *model.Single) error
	FindSingleByID(id string) (*model.Single, error)
	FindAllSingles(offset, limit int64, field string, desc bool) ([]*model.Single, error)
	FindAllSinglesByAdmin(admin string) (map[string]string, error)
	// Quick
	// FindQuickByID(id string) (*model.Quick, error)
	// UpdateQuick(single *model.Quick) error
	// FindQuickByID(id string) (*model.Quick, error)
	// FindAllQuicks(offset, limit int64, desc bool) ([]*model.Quick, error)
	// FindAllQuicksByAdmin(admin string) (map[string]string, error)
	// DeleteAllQuicks() error
}
