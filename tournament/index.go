package tournament

import (
	"sync"
	"time"
)

const (
	limitIndexRecords = 500

	TypeIndexTournament = 1
	TypeIndexSingle     = 2
)

// indexRecord ...
type indexRecord struct {
	Type  uint64 `json:"type"`
	ID    string `json:"id"`
	Name  string `json:"name"`
	Admin string `json:"admin"`
	Time  int64  `json:"time"`
}

type indexStore struct {
	data []*indexRecord
	mu   sync.Mutex
}

func newIndexStore() *indexStore {
	return &indexStore{
		data: make([]*indexRecord, 0, limitIndexRecords),
	}
}

func (i indexRecord) validate() error {
	return nil
}

func (s *indexStore) add(_type uint64, id, name, admin string, time int64) error {
	s.mu.Lock()
	it := &indexRecord{
		Type:  _type,
		ID:    id,
		Name:  name,
		Admin: admin,
		Time:  time,
	}
	if err := it.validate(); err != nil {
		return err
	}

	s.data = append([]*indexRecord{it}, s.data...)
	s.mu.Unlock()

	return nil
}

func (s *indexStore) getAll() []*indexRecord {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.data
}

func (s *indexStore) count() int {
	s.mu.Lock()
	defer s.mu.Unlock()
	return len(s.data)
}

func (s *indexStore) runGC() {
	ticker := time.NewTicker(time.Hour)
	for range ticker.C {
		go s.clean()
	}
}

func (s *indexStore) clean() {
	s.mu.Lock()
	defer s.mu.Unlock()

	if len(s.data) <= limitIndexRecords+1 {
		return
	}

	s.data = s.data[0:limitIndexRecords]
}
