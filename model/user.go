package model

import (
	"fmt"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/globalsign/mgo/bson"
	"golang.org/x/crypto/bcrypt"
)

const (
	typeUserDefault = 0

	minLengthUserName = 2
	maxLengthUserName = 32

	minLengthUserPassword = 2
	maxLengthUserPassword = 128
)

var (
	nameUserRegexp = regexp.MustCompile(`^[\p{L} 0-9_\-]+$`)
)

// User ...
type User struct {
	_ID               bson.ObjectId `bson:"_id" json:"_id,omitempty"`
	Name              string        `json:"name" bson:"name"`
	Email             string        `json:"email" bson:"email"`
	Password          string        `json:"password,omitempty" bson:"password"`
	EncryptedPassword string        `json:"-" bson:"encrypted_password"`
	CreateAt          int64         `json:"-" bson:"create_at"`
	Type              uint64        `json:"-" bson:"type"`
}

// Validate ...
func (u *User) Validate() error {
	if err := validateName(u.Name); err != nil {
		return err
	}
	if err := validatePassword(u.Password); err != nil {
		return err
	}

	return nil
}

func validateName(name string) error {
	name = strings.TrimSpace(name)
	lengthName := utf8.RuneCountInString(name)
	if lengthName < minLengthUserName {
		return fmt.Errorf("Длинна имени должна быть больше %v символов (сейчас: %v).", minLengthUserName, lengthName)
	}
	if lengthName > maxLengthUserName {
		return fmt.Errorf("Длинна имени должна быть меньше чем %v символов (сейчас: %v).", maxLengthUserName, lengthName)
	}
	if !nameUserRegexp.MatchString(name) {
		return fmt.Errorf("имя содержит недопустимые символы")
	}
	return nil
}

func validatePassword(pass string) error {
	lengthPass := utf8.RuneCountInString(pass)
	if lengthPass < minLengthUserPassword {
		return fmt.Errorf("Длинна пароля должна быть больше %v символов (сейчас: %v).", minLengthUserPassword, lengthPass)
	}
	if lengthPass > maxLengthUserPassword {
		return fmt.Errorf("Длинна пароля должна быть меньше чем %v символов (сейчас: %v).", maxLengthUserPassword, lengthPass)
	}
	return nil
}

// BeforeCreate ...
func (u *User) BeforeCreate() error {
	if len(u.Password) > 0 {
		b, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
		if err != nil {
			return err
		}
		u.EncryptedPassword = string(b)
	}
	u.Password = ""
	u.CreateAt = time.Now().Unix()
	return nil
}

// ComparePassword ...
func (u *User) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.EncryptedPassword), []byte(password)) == nil
}
