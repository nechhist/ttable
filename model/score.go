package model

import (
	"fmt"
	"time"
)

var (
	TypeScoreCircle uint64 = 1
	TypeScoreGroup  uint64 = 2
)

// Score ...
type Score struct {
	Score1     uint64 `json:"score1" bson:"score1"`
	Score2     uint64 `json:"score2" bson:"score2"`
	TimeUpdate int64  `json:"-" bson:"time_update"`
}

// Validate ...
func (s *Score) Validate() error {
	if s.Score1 < minUnitScore {
		return fmt.Errorf("score is less than %v (actually: %v)", minUnitScore, s.Score1)
	}
	if s.Score1 > maxUnitScore {
		return fmt.Errorf("score is more than %v (actually: %v)", maxUnitScore, s.Score1)
	}
	if s.Score2 < minUnitScore {
		return fmt.Errorf("score is less than %v (actually: %v)", minUnitScore, s.Score2)
	}
	if s.Score2 > maxUnitScore {
		return fmt.Errorf("score is more than %v (actually: %v)", maxUnitScore, s.Score2)
	}

	return nil
}

// Format ...
func (s Score) Format() Score {
	return s
}

// BeforeUpdate ...
func (s Score) BeforeUpdate() Score {
	s.TimeUpdate = time.Now().Unix()
	return s
}

// GetExampleScore ...
func GetExampleScore() Score {
	return Score{
		Score1: 1,
		Score2: 1,
	}
}
