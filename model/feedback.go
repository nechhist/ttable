package model

import (
	"fmt"
	"time"
	"unicode/utf8"

	"github.com/globalsign/mgo/bson"
)

var (
	MaxLengthTextFeedback = 1024
	MinLengthTextFeedback = 1
)

// Feedback ...
type Feedback struct {
	_ID  bson.ObjectId `bson:"_id" json:"_id,omitempty"`
	Name string        `json:"name" bson:"name"`
	Text string        `json:"text" bson:"text"`
	Data int64         `json:"data" bson:"data"`
}

// Validate ...
func (f *Feedback) Validate() error {
	lengthText := utf8.RuneCountInString(f.Text)
	if lengthText < MinLengthTextFeedback {
		return fmt.Errorf("text is less than %v characters long (actually: %v)", MinLengthTextFeedback, lengthText)
	}
	if lengthText > MaxLengthTextFeedback {
		return fmt.Errorf("text is more than %v characters long (actually: %v)", MaxLengthTextFeedback, lengthText)
	}
	return nil
}

// BeforeUpdate ...
func (f *Feedback) BeforeCreate() {
	f.Data = time.Now().Unix()
}
