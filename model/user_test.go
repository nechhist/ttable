package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUser_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		getUser func() *User
		isError bool
	}{
		{
			name: "valid",
			getUser: func() *User {
				return GetTestUser(t)
			},
			isError: false,
		},
		{
			name: "invalid name less",
			getUser: func() *User {
				tn := GetTestUser(t)
				tn.Name = ""
				return tn
			},
			isError: true,
		},
		{
			name: "invalid name more",
			getUser: func() *User {
				tn := GetTestUser(t)
				tn.Name = "1234567890123456789012345678901234567890"
				return tn
			},
			isError: true,
		},
		{
			name: "invalid pass less",
			getUser: func() *User {
				tn := GetTestUser(t)
				tn.Password = ""
				return tn
			},
			isError: true,
		},
		{
			name: "valid name with - _",
			getUser: func() *User {
				tn := GetTestUser(t)
				tn.Name = "n a_m-e2"
				return tn
			},
			isError: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getUser().Validate()
			assert.Equalf(t, tc.isError, err != nil, "error: %v", err)
		})
	}
}

// GetTestTournament ...
func GetTestUser(t *testing.T) *User {
	t.Helper()

	return &User{
		Name:     "test",
		Password: "test",
	}
}
