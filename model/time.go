package model

import (
	"fmt"
	"strings"
	"time"
	"unicode/utf8"
)

const (
	minLengthTimeValue = 0
	maxLengthTimeValue = 16
)

// Time ...
type Time struct {
	Value      string `json:"value" bson:"value"`
	TimeUpdate int64  `json:"-" bson:"time_update"`
}

// Validate ...
func (t *Time) Validate() error {
	lengthValue := utf8.RuneCountInString(t.Value)
	if lengthValue < minLengthTimeValue {
		return fmt.Errorf("value is less than %v characters long (actually: %v)", minLengthTimeValue, lengthValue)
	}
	if lengthValue > maxLengthTimeValue {
		return fmt.Errorf("value is more than %v characters long (actually: %v)", maxLengthTimeValue, lengthValue)
	}

	return nil
}

// Format ...
func (t Time) Format() Time {
	t.Value = strings.TrimSpace(t.Value)
	return t
}

// BeforeUpdate ...
func (t Time) BeforeUpdate() Time {
	t.TimeUpdate = time.Now().Unix()
	return t
}

// GetExampleTime ...
func GetExampleTime() Time {
	return Time{
		Value: "01.01.2020 22:00",
	}
}
