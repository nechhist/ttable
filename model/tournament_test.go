package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTournament_Validate(t *testing.T) {
	testCases := []struct {
		name          string
		getTournament func() *Tournament
		isError       bool
	}{
		{
			name: "valid",
			getTournament: func() *Tournament {
				return GetTestTournament(t)
			},
			isError: true,
		},
		{
			name: "invalid name less",
			getTournament: func() *Tournament {
				tn := GetTestTournament(t)
				tn.Name = ""
				return tn
			},
			isError: false,
		},
		{
			name: "invalid name more",
			getTournament: func() *Tournament {
				tn := GetTestTournament(t)
				tn.Name = "12345678901234567890"
				return tn
			},
			isError: false,
		},
		{
			name: "valid game",
			getTournament: func() *Tournament {
				tn := GetTestTournament(t)
				tn.Game = ""
				return tn
			},
			isError: true,
		},
		{
			name: "invalid game less",
			getTournament: func() *Tournament {
				tn := GetTestTournament(t)
				tn.Game = "1345678901234567890"
				return tn
			},
			isError: false,
		},
		{
			name: "valid description",
			getTournament: func() *Tournament {
				tn := GetTestTournament(t)
				tn.Description = ""
				return tn
			},
			isError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getTournament().Validate()
			assert.Equalf(t, tc.isError, err == nil, "error:%v", err)
		})
	}
}

// GetTestTournament ...
func GetTestTournament(t *testing.T) *Tournament {
	t.Helper()

	return &Tournament{
		Name:        "test",
		Game:        "game",
		Description: "desc",
	}
}
