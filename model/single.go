package model

import (
	"fmt"
	"strings"
	"time"
	"unicode/utf8"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	minLengthSingleName = 2
	maxLengthSingleName = 32

	minLengthSingleDescription = 0
	maxLengthSingleDescription = 512
)

// Single ...
type Single struct {
	ID          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Admin       string             `json:"admin" bson:"admin"`
	Type        uint64             `json:"type" bson:"type"`
	Name        string             `json:"name" bson:"name"`
	Description string             `json:"description" bson:"description"`
	Table       string             `json:"table" bson:"table"`
	Status      uint64             `json:"status" bson:"status"`
	ScoreEnable uint64             `json:"score_enable" bson:"score_enable"`
	TimeCreate  int64              `json:"time_create" bson:"time_create"`
	TimeUpdate  int64              `json:"time_update" bson:"time_update"`
	Units       map[string]Unit    `json:"units" bson:"units"`
	Times       map[string]Time    `json:"times" bson:"times"`
	Scores      map[string]Score   `json:"scores" bson:"scores"`
}

// Validate ...
func (s *Single) Validate() error {
	lengthName := utf8.RuneCountInString(s.Name)
	if lengthName < minLengthSingleName {
		return fmt.Errorf("name is less than %v characters long (actually: %v)", minLengthSingleName, lengthName)
	}
	if lengthName > maxLengthSingleName {
		return fmt.Errorf("name is more than %v characters long (actually: %v)", maxLengthSingleName, lengthName)
	}

	lengthDescription := utf8.RuneCountInString(s.Description)
	if lengthDescription < minLengthSingleDescription {
		return fmt.Errorf("description is less than %v characters long (actually: %v)", minLengthSingleDescription, lengthDescription)
	}
	if lengthDescription > maxLengthSingleDescription {
		return fmt.Errorf("description is more than %v characters long (actually: %v)", maxLengthSingleDescription, lengthDescription)
	}

	lengthTable := utf8.RuneCountInString(s.Table)
	if lengthTable < minLengthTable {
		return fmt.Errorf("table is less than %v characters long (actually: %v)", minLengthTable, lengthTable)
	}
	if lengthTable > maxLengthTable {
		return fmt.Errorf("table is more than %v characters long (actually: %v)", maxLengthTable, lengthTable)
	}

	return nil
}

// Format ...
func (s *Single) Format() {
	s.Admin = strings.TrimSpace(s.Admin)
	s.Name = strings.TrimSpace(s.Name)
	s.Description = strings.TrimSpace(s.Description)
	s.Table = strings.TrimSpace(s.Table)
}

// BeforeCreate ...
func (s *Single) BeforeCreate() {
	s.TimeCreate = time.Now().Unix()
	s.TimeUpdate = time.Now().Unix()
}

// BeforeUpdate ...
func (s *Single) BeforeUpdate() {
	s.TimeUpdate = time.Now().Unix()
}
