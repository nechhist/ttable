package model

import (
	"fmt"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/globalsign/mgo/bson"
)

const (
	minLengthTournamentName = 2
	maxLengthTournamentName = 16

	minLengthTournamentGame = 0
	maxLengthTournamentGame = 16

	minLengthTournamentDescription = 0
	maxLengthTournamentDescription = 512
)

var (
	nameTournamentRegexp = regexp.MustCompile(`^[\p{L} 0-9_\-]+$`)
)

// Tournament ...
type Tournament struct {
	_ID         bson.ObjectId     `bson:"_id" json:"_id,omitempty"`
	ID          uint64            `json:"id" bson:"id"`
	Name        string            `json:"name" bson:"name"`
	Type        uint64            `json:"type" bson:"type"`
	Admin       string            `json:"admin" bson:"admin"`
	Game        string            `json:"game" bson:"game"`
	Description string            `json:"description" bson:"description"`
	Logo        string            `json:"logo" bson:"logo"`
	Seasons     map[uint64]Season `json:"seasons" bson:"seasons"`

	LineScheme      uint64 `json:"-" bson:"line_scheme"`
	ColorScheme     uint64 `json:"-" bson:"color_scheme"`
	BackgroundImage string `json:"-" bson:"background_image"`
	Status          uint64 `json:"-" bson:"status"`
	TimeCreate      int64  `json:"-" bson:"time_create"`
	TimeUpdate      int64  `json:"-" bson:"time_update"`
}

// Validate ...
func (t *Tournament) Validate() error {
	lengthName := utf8.RuneCountInString(t.Name)
	if lengthName < minLengthTournamentName {
		return fmt.Errorf("name is less than %v characters long (actually: %v)", minLengthTournamentName, lengthName)
	}
	if lengthName > maxLengthTournamentName {
		return fmt.Errorf("name is more than %v characters long (actually: %v)", maxLengthTournamentName, lengthName)
	}

	lengthGame := utf8.RuneCountInString(t.Game)
	if lengthGame < minLengthTournamentGame {
		return fmt.Errorf("game is less than %v characters long (actually: %v)", minLengthTournamentGame, lengthGame)
	}
	if lengthGame > maxLengthTournamentGame {
		return fmt.Errorf("game is more than %v characters long (actually: %v)", maxLengthTournamentGame, lengthGame)
	}

	lengthDescription := utf8.RuneCountInString(t.Description)
	if lengthDescription < minLengthTournamentDescription {
		return fmt.Errorf("description is less than %v characters long (actually: %v)", minLengthTournamentDescription, lengthDescription)
	}
	if lengthDescription > maxLengthTournamentDescription {
		return fmt.Errorf("description is more than %v characters long (actually: %v)", maxLengthTournamentDescription, lengthDescription)
	}
	return nil
}

// Format ...
func (t *Tournament) Format() {
	t.Name = strings.TrimSpace(t.Name)
	t.Admin = strings.TrimSpace(t.Admin)
	t.Game = strings.TrimSpace(t.Game)
	t.Description = strings.TrimSpace(t.Description)
	t.Logo = strings.TrimSpace(t.Logo)
	t.BackgroundImage = strings.TrimSpace(t.BackgroundImage)
}

// BeforeCreate ...
func (t *Tournament) BeforeCreate() {
	t.TimeCreate = time.Now().Unix()
	t.TimeUpdate = time.Now().Unix()
}

// BeforeUpdate ...
func (t *Tournament) BeforeUpdate() {
	t.TimeUpdate = time.Now().Unix()
}

// GetExampleTournament ...
func GetExampleTournament() *Tournament {
	return &Tournament{
		ID:          1,
		Name:        "FirstLiga 1",
		Admin:       "admin",
		Game:        "dota2",
		Description: "this first game",
		Seasons:     make(map[uint64]Season),
	}
}
