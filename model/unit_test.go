package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnit_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		getUnit func() *Unit
		isError bool
	}{
		{
			name: "valid",
			getUnit: func() *Unit {
				return GetTestUnit(t)
			},
			isError: true,
		},
		// value
		{
			name: "valid name empty",
			getUnit: func() *Unit {
				s := GetTestUnit(t)
				s.Value = ""
				return s
			},
			isError: true,
		},
		{
			name: "invalid name more",
			getUnit: func() *Unit {
				s := GetTestUnit(t)
				s.Value = "01234567890134567"
				return s
			},
			isError: false,
		},
		// score
		{
			name: "valid score",
			getUnit: func() *Unit {
				s := GetTestUnit(t)
				s.Score = 99
				return s
			},
			isError: true,
		},
		{
			name: "invalid score",
			getUnit: func() *Unit {
				s := GetTestUnit(t)
				s.Score = 999
				return s
			},
			isError: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getUnit().Validate()
			assert.Equalf(t, tc.isError, err == nil, "error:%v", err)
		})
	}
}

// GetTestUnit ...
func GetTestUnit(t *testing.T) *Unit {
	t.Helper()

	return &Unit{
		Value: "test",
		Score: 1,
	}
}
