package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSeason_Validate(t *testing.T) {
	testCases := []struct {
		name      string
		getSeason func() *Season
		isError   bool
	}{
		{
			name: "valid",
			getSeason: func() *Season {
				return GetTestSeason(t)
			},
			isError: true,
		},
		// name
		{
			name: "invalid name less",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.Name = ""
				return tn
			},
			isError: false,
		},
		{
			name: "invalid name more",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.Name = "01234567890134567"
				return tn
			},
			isError: false,
		},
		{
			name: "valid name",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.Name = "абвгд абвгд абвг"
				return tn
			},
			isError: true,
		},
		// table
		{
			name: "invalid table less",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.Table = ""
				return tn
			},
			isError: false,
		},
		{
			name: "invalid table more",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.Table = "0123456789013456 0123456789013456"
				return tn
			},
			isError: false,
		},
		// TimeAction
		{
			name: "valid TimeAction",
			getSeason: func() *Season {
				tn := GetTestSeason(t)
				tn.TimeAction = ""
				return tn
			},
			isError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getSeason().Validate()
			assert.Equalf(t, tc.isError, err == nil, "error:%v", err)
		})
	}
}

// GetTestSeason ...
func GetTestSeason(t *testing.T) *Season {
	t.Helper()

	return &Season{
		Name:       "test",
		TimeAction: "test",
		Table:      "test",
	}
}
