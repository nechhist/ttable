package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSingle_Validate(t *testing.T) {
	testCases := []struct {
		name      string
		getSingle func() *Single
		isError   bool
	}{
		{
			name: "valid",
			getSingle: func() *Single {
				return GetTestSingle(t)
			},
			isError: true,
		},
		// name
		{
			name: "invalid name less",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Name = ""
				return s
			},
			isError: false,
		},
		{
			name: "invalid name more",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Name = "012345678901345678901234567890123456"
				return s
			},
			isError: false,
		},
		{
			name: "valid name",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Name = "абвгд абвгд абвг"
				return s
			},
			isError: true,
		},
		// table
		{
			name: "invalid table less",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Table = ""
				return s
			},
			isError: false,
		},
		{
			name: "invalid table more",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Table = "0123456789013456 0123456789013456"
				return s
			},
			isError: false,
		},
		// Description
		{
			name: "valid Description",
			getSingle: func() *Single {
				s := GetTestSingle(t)
				s.Description = ""
				return s
			},
			isError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getSingle().Validate()
			assert.Equalf(t, tc.isError, err == nil, "error:%v", err)
		})
	}
}

// GetTestSeason ...
func GetTestSingle(t *testing.T) *Single {
	t.Helper()

	return &Single{
		Name:        "test",
		Table:       "test",
		Description: "test",
	}
}
