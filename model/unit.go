package model

import (
	"fmt"
	"strings"
	"time"
	"unicode/utf8"
)

const (
	minLengthUnit = 0
	maxLengthUnit = 16

	minUnitScore = 0
	maxUnitScore = 99
)

// Unit ...
type Unit struct {
	Value      string `json:"value" bson:"value"`
	Score      uint64 `json:"score" bson:"score"`
	Logo       string `json:"logo" bson:"logo"`
	TimeUpdate int64  `json:"-" bson:"time_update"`
}

// Format ...
func (u Unit) Format() Unit {
	u.Value = strings.TrimSpace(u.Value)
	return u
}

// Validate ...
func (u *Unit) Validate() error {
	lengthValue := utf8.RuneCountInString(u.Value)
	if lengthValue < minLengthUnit {
		return fmt.Errorf("value is less than %v characters long (actually: %v)", minLengthUnit, lengthValue)
	}
	if lengthValue > maxLengthUnit {
		return fmt.Errorf("value is more than %v characters long (actually: %v)", maxLengthUnit, lengthValue)
	}

	if u.Score < minUnitScore {
		return fmt.Errorf("score is less than %v (actually: %v)", minUnitScore, u.Score)
	}
	if u.Score > maxUnitScore {
		return fmt.Errorf("score is more than %v (actually: %v)", maxUnitScore, u.Score)
	}

	return nil
}

// BeforeUpdate ...
func (u Unit) BeforeUpdate() Unit {
	u.TimeUpdate = time.Now().Unix()
	return u
}
