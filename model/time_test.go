package model

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTime_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		getTime func() *Time
		isError bool
	}{
		{
			name: "valid",
			getTime: func() *Time {
				return GetTimeUnit(t)
			},
			isError: true,
		},
		// value
		{
			name: "valid name empty",
			getTime: func() *Time {
				s := GetTimeUnit(t)
				s.Value = ""
				return s
			},
			isError: true,
		},
		{
			name: "invalid name more",
			getTime: func() *Time {
				s := GetTimeUnit(t)
				s.Value = "01234567890134567"
				return s
			},
			isError: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.getTime().Validate()
			assert.Equalf(t, tc.isError, err == nil, "error:%v", err)
		})
	}
}

// GetTestUnit ...
func GetTimeUnit(t *testing.T) *Time {
	t.Helper()

	return &Time{
		Value: "test",
	}
}
