package model

// Counter ...
type Counter struct {
	Hits       uint64            `json:"hits"`
	Hosts      map[string]uint64 `json:"hosts"`
	TimeUpdate int64             `json:"time_update"`
}
