package model

import (
	"strings"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

// Quick ...
type Quick struct {
	Admin       string          `json:"admin" bson:"admin"`
	Type        uint64          `json:"type" bson:"type"`
	Name        string          `json:"name" bson:"name"`
	Description  string          `json:"description" bson:"description"`
	Table       string          `json:"table" bson:"table"`
	Status      uint64          `json:"status" bson:"status"`
	ScoreEnable uint64          `json:"score_enable" bson:"score_enable"`
	TimeCreate  int64           `json:"time_create" bson:"time_create"`
	TimeUpdate  int64           `json:"time_update" bson:"time_update"`
	Units       map[string]Unit `json:"units" bson:"units"`
	Times       map[string]Time `json:"times" bson:"times"`
}

// Validate ...
func (q *Quick) Validate() error {
	return validation.ValidateStruct(
		q,
		validation.Field(&q.Admin, validation.Required),
		validation.Field(&q.Name, validation.Required, validation.Length(4, 16)),
		validation.Field(&q.Table, validation.Required, validation.Length(4, 16)),
	)
}

// Format ...
func (q Quick) Format() Quick {
	q.Admin = strings.TrimSpace(q.Admin)
	q.Name = strings.TrimSpace(q.Name)
	q.Description = strings.TrimSpace(q.Description)
	q.Table = strings.TrimSpace(q.Table)
	return q
}

// BeforeCreate ...
func (q Quick) BeforeCreate() Quick {
	q.TimeCreate = time.Now().Unix()
	q.TimeUpdate = time.Now().Unix()
	return q
}

// BeforeUpdate ...
func (q Quick) BeforeUpdate() Quick {
	q.TimeUpdate = time.Now().Unix()
	return q
}
