package model

import (
	"fmt"
	"strings"
	"time"
	"unicode/utf8"
)

const (
	minLengthSeasonName = 2
	maxLengthSeasonName = 16

	minLengthSeasonTimeAction = 0
	maxLengthSeasonTimeAction = 32
)

// Season ...
type Season struct {
	Type        uint64           `json:"type" bson:"type"`
	Name        string           `json:"name" bson:"name"`
	TimeAction  string           `json:"time_action" bson:"time_action"`
	Table       string           `json:"table" bson:"table"`
	Status      uint64           `json:"status" bson:"status"`
	ScoreEnable uint64           `json:"score_enable" bson:"score_enable"`
	TimeCreate  int64            `json:"time_create" bson:"time_create"`
	TimeUpdate  int64            `json:"time_update" bson:"time_update"`
	Units       map[string]Unit  `json:"units" bson:"units"`
	Times       map[string]Time  `json:"times" bson:"times"`
	Scores      map[string]Score `json:"scores" bson:"scores"`
}

// Validate ...
func (s *Season) Validate() error {
	lengthName := utf8.RuneCountInString(s.Name)
	if lengthName < minLengthSeasonName {
		return fmt.Errorf("name is less than %v characters long (actually: %v)", minLengthSeasonName, lengthName)
	}
	if lengthName > maxLengthSeasonName {
		return fmt.Errorf("name is more than %v characters long (actually: %v)", maxLengthSeasonName, lengthName)
	}

	lengthTimeAction := utf8.RuneCountInString(s.TimeAction)
	if lengthTimeAction < minLengthSeasonTimeAction {
		return fmt.Errorf("time_action is less than %v characters long (actually: %v)", minLengthSeasonTimeAction, lengthTimeAction)
	}
	if lengthTimeAction > maxLengthSeasonTimeAction {
		return fmt.Errorf("time_action is more than %v characters long (actually: %v)", maxLengthSeasonTimeAction, lengthTimeAction)
	}

	lengthTable := utf8.RuneCountInString(s.Table)
	if lengthTable < minLengthTable {
		return fmt.Errorf("table is less than %v characters long (actually: %v)", minLengthTable, lengthTable)
	}
	if lengthTable > maxLengthTable {
		return fmt.Errorf("table is more than %v characters long (actually: %v)", maxLengthTable, lengthTable)
	}

	return nil
}

// Format ...
func (s Season) Format() Season {
	s.Name = strings.TrimSpace(s.Name)
	s.TimeAction = strings.TrimSpace(s.TimeAction)
	s.Table = strings.TrimSpace(s.Table)
	return s
}

// BeforeCreate ...
func (s Season) BeforeCreate() Season {
	s.TimeCreate = time.Now().Unix()
	s.TimeUpdate = time.Now().Unix()
	return s
}

// BeforeUpdate ...
func (s Season) BeforeUpdate() Season {
	s.TimeUpdate = time.Now().Unix()
	return s
}
