package counter

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"go.uber.org/zap/zapcore"
)

func TestService_HandleSetGetCode(t *testing.T) {
	s := getTestServer(t)

	testCases := []struct {
		nameCase string
		path     string
		method   string
		code     int
	}{
		{
			nameCase: "post ok",
			path:     urlAPIv1 + "/tournament/1",
			method:   http.MethodPost,
			code:     http.StatusOK,
		},
		{
			nameCase: "get ok",
			path:     urlAPIv1 + "/tournament/1",
			method:   http.MethodGet,
			code:     http.StatusOK,
		},
		{
			nameCase: "get not found id",
			path:     urlAPIv1 + "/tournament/2",
			method:   http.MethodGet,
			code:     http.StatusNotFound,
		},
		{
			nameCase: "get bad id",
			path:     urlAPIv1 + "/tournament/aaa",
			method:   http.MethodGet,
			code:     http.StatusNotFound,
		},
		{
			nameCase: "post ok string",
			path:     urlAPIv1 + "/tournament/aaa",
			method:   http.MethodPost,
			code:     http.StatusOK,
		},
		{
			nameCase: "get without id",
			path:     urlAPIv1 + "/tournament",
			method:   http.MethodGet,
			code:     http.StatusNotFound,
		},
		{
			nameCase: "post without id",
			path:     urlAPIv1 + "/tournament",
			method:   http.MethodPost,
			code:     http.StatusNotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.nameCase, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(tc.method, tc.path, nil)
			s.httpServer.Handler.ServeHTTP(rec, req)
			assert.Equalf(t, tc.code, rec.Code, "path %v", tc.path)
		})
	}
}

func TestService_HandleSetGetRequest(t *testing.T) {
	s := getTestServer(t)
	rec := httptest.NewRecorder()
	counterRequest := &CounterRequest{}

	// set
	req, _ := http.NewRequest(http.MethodPost, urlAPIv1+"/tournament/1", nil)
	s.httpServer.Handler.ServeHTTP(rec, req)
	if err := json.NewDecoder(rec.Body).Decode(counterRequest); err != nil {
		t.Error(err)
	}
	assert.Equalf(t, counterRequest.Hits, uint64(1), "counterRequest.Hits: %v", counterRequest.Hits)
	assert.Equalf(t, counterRequest.Hosts, uint64(1), "counterRequest.Hosts: %v", counterRequest.Hosts)

	// get
	req, _ = http.NewRequest(http.MethodGet, urlAPIv1+"/tournament/1", nil)
	s.httpServer.Handler.ServeHTTP(rec, req)
	if err := json.NewDecoder(rec.Body).Decode(counterRequest); err != nil {
		t.Error(err)
	}
	assert.Equalf(t, counterRequest.Hits, uint64(1), "counterRequest.Hits: %v", counterRequest.Hits)
	assert.Equalf(t, counterRequest.Hosts, uint64(1), "counterRequest.Hosts: %v", counterRequest.Hosts)
}

func getTestServer(t *testing.T) *Service {
	t.Helper()

	s := &Service{}
	s.store = NewStore()
	s.config = &Config{
		TokenKey:       "test",
		AllowedOrigins: []string{"*"},
	}
	s.logger = log.New(zapcore.DebugLevel)
	s.httpServer = &http.Server{
		Handler: s.getRouters(),
	}

	return s
}
