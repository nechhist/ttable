package counter

import (
	"fmt"
	"net/http"
	"runtime/debug"
	"time"
)

// responseWriter
type responseWriter struct {
	http.ResponseWriter
	code int
}

// errorMiddleware
func (s *Service) errorMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			l := s.logger.With(r.Context())
			if e := recover(); e != nil {
				var ok bool
				var err error
				if err, ok = e.(error); !ok {
					err = fmt.Errorf("%v", e)
				}
				if err != nil {
					s.response(w, r, http.StatusInternalServerError, err, "errorMiddleware")
				}
				l.Errorf("recovered from panic (%v): %s", err, debug.Stack())
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// accessMiddleware
func (s *Service) accessMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		s.logger.Infof(
			"Access Log: %s %s %s %d %dms",
			r.Method,
			r.URL.Path,
			r.Proto,
			rw.code,
			time.Since(start).Milliseconds(),
		)
	})
}
