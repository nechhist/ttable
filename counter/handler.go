package counter

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	urlAPIv1 = "/api/v1"

	headersOk     = handlers.AllowedHeaders([]string{"content-type"})
	methodsOk     = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	credentialsOk = handlers.AllowCredentials()
)

// getRouters ...
func (s *Service) getRouters() *mux.Router {
	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler()).Methods("GET")
	router.HandleFunc("/healthcheck", s.handleHealthCheck()).Methods("GET")

	apiV1 := router.PathPrefix(urlAPIv1).Subrouter()
	apiV1.Use(s.accessMiddleware)
	apiV1.Use(s.errorMiddleware)
	apiV1.Use(handlers.CORS(credentialsOk, headersOk, handlers.AllowedOrigins(s.config.AllowedOrigins), methodsOk))

	// example: curl -i -X GET http://localhost:15800/api/v1/tournament/1
	apiV1.HandleFunc("/tournament/{id}", s.handleTournamentGet()).Methods("GET")
	// example: curl -i -X POST http://localhost:15800/api/v1/tournament/1
	apiV1.HandleFunc("/tournament/{id}", s.handleTournamentSet()).Methods("POST")

	// example: curl -i -X GET http://localhost:15800/api/v1/single/aaa
	apiV1.HandleFunc("/single/{id}", s.handleSingleGet()).Methods("GET")
	// example: curl -i -X POST http://localhost:15800/api/v1/single/aaa
	apiV1.HandleFunc("/single/{id}", s.handleSingleSet()).Methods("POST")

	// DUMMY
	apiV1.HandleFunc("/tournament/{id}", handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc("/single/{id}", handleDummy).Methods("OPTIONS")

	return router
}

// CounterRequest ...
type CounterRequest struct {
	Hits  uint64 `json:"hits"`
	Hosts uint64 `json:"hosts"`
}

// Tournament
func (s *Service) handleTournamentGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableStr(r, "id")
		if err != nil {
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentGet/getVariableInt")
			return
		}

		counter, err := s.store.get(id)
		if err != nil {
			s.response(w, r, http.StatusNotFound, err, "handleTournamentGet/store.get")
			return
		}

		counterRequest := CounterRequest{}
		counterRequest.Hits = counter.Hits
		counterRequest.Hosts = uint64(len(counter.Hosts))
		s.response(w, r, http.StatusOK, counterRequest, "handleTournamentGet")
	}
}

func (s *Service) handleTournamentSet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableStr(r, "id")
		if err != nil {
			s.response(w, r, http.StatusBadRequest, err, "handleTournamentSet/getVariableInt")
			return
		}

		ip, _, _ := net.SplitHostPort(r.RemoteAddr)

		counter := s.store.set(id, ip)

		counterRequest := CounterRequest{}
		counterRequest.Hits = counter.Hits
		counterRequest.Hosts = uint64(len(counter.Hosts))

		s.response(w, r, http.StatusOK, counterRequest, "handleTournamentSet")
	}
}

// Single
func (s *Service) handleSingleGet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableStr(r, "id")
		if err != nil {
			s.response(w, r, http.StatusBadRequest, err, "handleSingleGet/getVariableInt")
			return
		}

		counter, err := s.store.get(id)
		if err != nil {
			s.response(w, r, http.StatusNotFound, err, "handleSingleGet/store.get")
			return
		}

		counterRequest := CounterRequest{}
		counterRequest.Hits = counter.Hits
		counterRequest.Hosts = uint64(len(counter.Hosts))
		s.response(w, r, http.StatusOK, counterRequest, "handleSingleGet")
	}
}

func (s *Service) handleSingleSet() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := getVariableStr(r, "id")
		if err != nil {
			s.response(w, r, http.StatusBadRequest, err, "handleSingleSet/getVariableInt")
			return
		}

		ip, _, _ := net.SplitHostPort(r.RemoteAddr)

		counter := s.store.set(id, ip)

		counterRequest := CounterRequest{}
		counterRequest.Hits = counter.Hits
		counterRequest.Hosts = uint64(len(counter.Hosts))

		s.response(w, r, http.StatusOK, counterRequest, "handleSingleSet")
	}
}

func (s *Service) handleHealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data := struct {
			Status string
		}{
			"ok",
		}
		s.response(w, r, http.StatusOK, data, "")
	}
}

func handleDummy(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello Dummy!")
}

var metricRequests = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Total number of HTTP requests by status code end method.",
	},
	[]string{"handler", "code"},
)
var metricLabels = prometheus.Labels{}

// response ...
func (s *Service) response(w http.ResponseWriter, r *http.Request, code int, data interface{}, handlerName string) {
	if handlerName != "" {
		metricLabels["code"] = strconv.Itoa(code)
		metricLabels["handler"] = handlerName
		metricRequests.With(metricLabels).Inc()
	}

	w.WriteHeader(code)
	if err, ok := data.(error); ok {
		data = map[string]string{"error": err.Error()}
	}
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
	//s.logger.Debug("Response. Code:", code, ". Header: ", w.Header(), ". Data: ", data)
}
