package counter

import (
	"context"
	"fmt"
	"net/http"
	"time"

	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/shutdown"
	"gitlab.com/nechhist/ttable/pkg/telegram"
)

const (
	// Version app
	Version = "1.0.0"
)

// Service ...
type Service struct {
	config     *Config
	httpServer *http.Server
	logger     log.Logger
	store      *Store
	telegram   *telegram.Bot
}

// New ...
func New(cfg *Config, store *Store, tBot *telegram.Bot, logger log.Logger) *Service {
	s := &Service{}
	s.store = store
	s.logger = logger
	s.telegram = tBot
	s.config = cfg
	s.httpServer = &http.Server{
		Addr:         fmt.Sprintf(":%v", cfg.ServerPort),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      s.getRouters(),
	}
	return s
}

// ListenAndServe ...
func (s *Service) ListenAndServe() error {
	return s.httpServer.ListenAndServe()
}

// Shutdown ...
func (s *Service) Shutdown() error {
	return shutdown.Shutdown(s.httpServer, 2*time.Second)
}

// RunSaving ...
func (s *Service) RunSavingAndCleaning(ctx context.Context) error {
	tickerSaving := time.NewTicker(time.Second * time.Duration(s.config.PeriodSavingSec))
	defer tickerSaving.Stop()

	tickerCleaning := time.NewTicker(time.Second * time.Duration(s.config.PeriodCleaningSec))
	defer tickerCleaning.Stop()

	for {
		select {
		case <-ctx.Done():
			return nil
		case <-tickerSaving.C:
			s.logger.Debug("RunSaving worked")
			if err := s.store.save(s.config.PathFile); err != nil {
				s.logger.Error(err)
			}
		case <-tickerCleaning.C:
			s.logger.Debug("RunGC worked")
			s.store.cleaning(s.config.TtlCounter)
		}
	}
}
