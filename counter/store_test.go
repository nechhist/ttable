package counter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStore_SetGet(t *testing.T) {
	s := NewStore()

	// set
	c := s.set("1", "1:1:1:1")
	assert.Equal(t, c.Hits, uint64(1))
	assert.Equal(t, c.Hosts, map[string]uint64{
		"1:1:1:1": uint64(1),
	})

	// set other ip
	c = s.set("1", "2:2:2:2")
	assert.Equal(t, c.Hits, uint64(2))
	assert.Equal(t, c.Hosts, map[string]uint64{
		"1:1:1:1": uint64(1),
		"2:2:2:2": uint64(1),
	})

	// set other some ip
	c = s.set("1", "2:2:2:2")
	assert.Equal(t, c.Hits, uint64(3))
	assert.Equal(t, c.Hosts, map[string]uint64{
		"1:1:1:1": uint64(1),
		"2:2:2:2": uint64(2),
	})

	// get
	c, err := s.get("1")
	assert.Nil(t, err)
	assert.Equal(t, c.Hits, uint64(3))
	assert.Equal(t, c.Hosts, map[string]uint64{
		"1:1:1:1": uint64(1),
		"2:2:2:2": uint64(2),
	})
}
