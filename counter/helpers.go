package counter

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func getVariableStr(r *http.Request, v string) (string, error) {
	result, ok := mux.Vars(r)[v]
	if !ok {
		return "", fmt.Errorf("variable %v not found", v)
	}
	if result == "" {
		return "", fmt.Errorf("error in the %v variable - is empty", v)
	}
	return result, nil
}
