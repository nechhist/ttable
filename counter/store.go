package counter

import (
	"encoding/gob"
	"errors"
	"fmt"
	"os"
	"sync"
	"time"

	"gitlab.com/nechhist/ttable/model"
)

var (
	errTournamentNotFound = errors.New("tournament not found")
)

// Store ...
type Store struct {
	data     map[string]*model.Counter
	mu       sync.Mutex
	pathFile string
}

// NewStore ...
func NewStore() *Store {
	s := &Store{}
	s.data = make(map[string]*model.Counter, 0)
	return s
}

func (s *Store) get(id string) (*model.Counter, error) {
	s.mu.Lock()
	counter, ok := s.data[id]
	s.mu.Unlock()

	if !ok {
		return nil, errTournamentNotFound
	}

	return counter, nil
}

func (s *Store) set(id string, ip string) *model.Counter {
	s.mu.Lock()

	counter, ok := s.data[id]
	if !ok {
		counter = &model.Counter{
			Hosts: make(map[string]uint64),
		}
	}

	counter.Hits++
	counter.TimeUpdate = time.Now().Unix()

	currentHost := counter.Hosts[ip]
	currentHost++
	counter.Hosts[ip] = currentHost

	s.data[id] = counter
	s.mu.Unlock()

	return counter
}

// Init ...
func (s *Store) Init(pathName string) error {
	f, err := os.Open(pathName)
	if err != nil {
		return fmt.Errorf("Error store init - os.Open, file: %v. Error message: %v", pathName, err)
	}
	defer f.Close()

	enc := gob.NewDecoder(f)
	if err := enc.Decode(&s.data); err != nil {
		return fmt.Errorf("Error store init - enc.Decode. Error message: %v", err)
	}
	return nil
}

func (s *Store) save(pathName string) error {
	f, err := os.Create(pathName)
	if err != nil {
		return fmt.Errorf("Error file save / Cant Create file %s. Error message: %v", pathName, err)
	}
	defer f.Close()

	enc := gob.NewEncoder(f)

	s.mu.Lock()
	if err := enc.Encode(s.data); err != nil {
		return fmt.Errorf("Error file save / Cant Decode. Error message: %v", err)
	}
	s.mu.Unlock()

	return nil
}

func (s *Store) cleaning(ttl int64) {
	var pointTime = time.Now().Unix() - ttl
	s.mu.Lock()
	for id, counter := range s.data {
		if counter.TimeUpdate < pointTime {
			delete(s.data, id)
		}
	}
	s.mu.Unlock()
}

func (s *Store) delete(id string) {
	s.mu.Lock()
	delete(s.data, id)
	s.mu.Unlock()
}

func (s *Store) statistic() (uint64, uint64, uint64) {
	var CountT = uint64(len(s.data))
	var CountHit uint64 = 0
	var CountHost uint64 = 0

	s.mu.Lock()
	for _, counter := range s.data {
		CountHit += counter.Hits
		CountHost += uint64(len(counter.Hosts))
	}
	s.mu.Unlock()

	return CountT, CountHit, CountHost
}
