package counter

import (
	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/kelseyhightower/envconfig"
)

// Config represents an application configuration.
type Config struct {
	LogLevel          string
	ServerPort        string
	AllowedOrigins    []string
	PathFile          string
	PeriodSavingSec   int64
	PeriodCleaningSec int64
	TtlCounter        int64
	TokenKey          string
	TelegramTokenBot  string
	TelegramChatID    string
}

// LoadConfig returns an application configuration.
func LoadConfig() (*Config, error) {
	c := &Config{
		LogLevel:          "debug",
		ServerPort:        "15802",
		PathFile:          "/app/data/counter.data",
		PeriodSavingSec:   60 * 60,
		PeriodCleaningSec: 60 * 60 * 24,
		TtlCounter:        60 * 60 * 24 * 120,
		TelegramTokenBot:  "184883614:AAHAthCjIqTcNPNL0TEVOXkEeDxdmANs-ac",
		TelegramChatID:    "-1001060781892",
		AllowedOrigins:    []string{"http://localhost:3000"},
		TokenKey:          "token_key",
	}
	envconfig.Process("app", c)

	if err := c.Validate(); err != nil {
		return nil, err
	}

	return c, nil
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		// Server
		validation.Field(&c.ServerPort, validation.Required),
		// AllowedOrigins
		validation.Field(&c.AllowedOrigins, validation.Required),
		// Token
		validation.Field(&c.TokenKey, validation.Required),
		// PathFile
		validation.Field(&c.PathFile, validation.Required),
		// PeriodSavingSec
		validation.Field(&c.PeriodSavingSec, validation.Required),
		// PeriodCleaningSec
		validation.Field(&c.PeriodCleaningSec, validation.Required),
		// TtlCounter
		validation.Field(&c.TtlCounter, validation.Required),
		// TokenBot
		validation.Field(&c.TelegramTokenBot, validation.Required),
		// ChatID
		validation.Field(&c.TelegramChatID, validation.Required),
	)
}
