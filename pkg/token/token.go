package token

import (
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

var (
	// ErrTimeOut ...
	ErrTimeOut = errors.New("token's time out")
)

// token ...
type token struct {
	payload string
	time    string
	hash    string
}

// Create ...
func Create(payload, key string) (string, error) {
	if len(payload) == 0 {
		return "", errors.New("token cant create, payload is empty")
	}
	if len(key) == 0 {
		return "", errors.New("token cant create, key is empty")
	}

	str := encode(payload, key)
	return str, nil
}

// encode ...
func encode(payload, key string) string {
	timeString := strconv.Itoa(int(time.Now().Unix()))
	hash := getHash(payload, timeString, key)

	payload_base64 := base64.StdEncoding.EncodeToString([]byte(payload))
	timeString_base64 := base64.StdEncoding.EncodeToString([]byte(timeString))
	hash_base64 := base64.StdEncoding.EncodeToString([]byte(hash))

	return payload_base64 + "." + timeString_base64 + "." + hash_base64
}

// GetPayload from token string
func GetPayload(tokenStr, key string, timeDelay int64) (string, error) {
	t, err := decode(tokenStr, key)
	if err != nil {
		return "", err
	}

	// check time
	now := time.Now().Unix()
	timeMin := now - timeDelay
	timeMax := now + timeDelay
	timeToken, _ := strconv.ParseInt(t.time, 10, 64)

	if timeToken < timeMin || timeToken > timeMax {
		return "", ErrTimeOut
	}

	return t.payload, nil
}

// decode ...
func decode(tokenStr, key string) (*token, error) {
	split := strings.Split(tokenStr, ".")
	if len(split) != 3 {
		return nil, errors.New("token string is broken, error split")
	}

	_payload, err := base64.StdEncoding.DecodeString(split[0])
	if err != nil {
		return nil, fmt.Errorf("token base64 decode error: %v", err)
	}
	_time, err := base64.StdEncoding.DecodeString(split[1])
	if err != nil {
		return nil, fmt.Errorf("token base64 decode error: %v", err)
	}
	_hash, err := base64.StdEncoding.DecodeString(split[2])
	if err != nil {
		return nil, fmt.Errorf("token base64 decode error: %v", err)
	}

	token := &token{
		payload: string(_payload),
		time:    string(_time),
		hash:    string(_hash),
	}

	// check hash
	hash := getHash(token.payload, token.time, key)
	if token.hash != hash {
		return nil, errors.New("token has incorrect hash-sum, token: " + tokenStr)
	}

	return token, nil
}

// getHash ...
func getHash(payload, time, key string) string {
	hash := sha256.Sum256([]byte(payload + time + key))
	return fmt.Sprintf("%s", hash)
}
