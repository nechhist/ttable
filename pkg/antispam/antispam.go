package antispam

import (
	"errors"
	"net"
	"sync"
	"time"
)

var (
	// ErrIPExists ...
	ErrIPExists = errors.New("IP exists")
)

// Store ...
type Store struct {
	ips   map[string]struct{}
	mu    sync.Mutex
	delay int64 // sec.
}

// NewStore ...
func NewStore(delay int64) *Store {
	return &Store{
		ips:   make(map[string]struct{}),
		delay: delay,
	}
}

// Add ...
func (s *Store) Add(addr string) error {
	if s.delay == 0 {
		return nil
	}

	s.mu.Lock()
	defer s.mu.Unlock()

	ip, _, _ := net.SplitHostPort(addr)

	if _, ok := s.ips[ip]; ok {
		return ErrIPExists
	}

	s.ips[ip] = struct{}{}
	return nil
}

// RunGC ...
func (s *Store) RunGC() {
	if s.delay == 0 {
		return
	}

	ticker := time.NewTicker(time.Duration(s.delay) * time.Second)
	for range ticker.C {
		go s.clean()
	}
}

// clean ...
func (s *Store) clean() {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.ips = make(map[string]struct{})
}
