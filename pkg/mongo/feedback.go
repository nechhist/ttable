package mongo

import (
	"context"

	"gitlab.com/nechhist/ttable/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (s *UserStore) CreateFeedback(f *model.Feedback) error {
	_, err := s.Feedback.InsertOne(context.TODO(), f)
	return err
}

func (s *UserStore) FindAllFeedback(offset, limit int64) ([]*model.Feedback, error) {
	result := make([]*model.Feedback, 0)

	ctx := context.TODO()
	cursor, err := s.Feedback.Find(context.TODO(), bson.D{}, &options.FindOptions{
		Limit: &limit,
		Skip:  &offset,
	})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var f = new(model.Feedback)
		if err = cursor.Decode(&f); err != nil {
			return nil, err
		}
		result = append(result, f)
	}

	return result, nil
}
