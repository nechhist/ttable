package mongo

import (
	"context"
	"errors"

	"gitlab.com/nechhist/ttable/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ErrSingleNotFound = errors.New("single not found")
)

func (s *TournamentStore) FindSingleByID(id string) (*model.Single, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.D{{"_id", objectID}}
	var result *model.Single
	err = s.Single.FindOne(context.TODO(), filter).Decode(&result)
	return result, err
}

func (s *TournamentStore) CreateSingle(single *model.Single) error {
	insertResult, err := s.Single.InsertOne(context.TODO(), single)
	if err != nil {
		return nil
	}

	id, ok := insertResult.InsertedID.(primitive.ObjectID)
	if !ok {
		return errors.New("dont convert InsertedID to ObjectID")
	}
	single.ID = id

	return err
}

func (s *TournamentStore) UpdateSingle(single *model.Single) error {
	filter := bson.D{{"_id", single.ID}}
	_, err := s.Single.ReplaceOne(context.TODO(), filter, single)
	return err
}

func (s *TournamentStore) FindAllSingles(offset, limit int64, field string, desc bool) ([]*model.Single, error) {
	result := make([]*model.Single, 0)
	option := &options.FindOptions{
		Limit: &limit,
		Skip:  &offset,
	}

	if field == "" {
		sort := 0
		if desc {
			sort = -1
		}
		option = &options.FindOptions{
			Limit: &limit,
			Skip:  &offset,
			Sort:  bson.D{{field, sort}},
		}
	}

	ctx := context.TODO()
	cursor, err := s.Single.Find(ctx, bson.D{}, option)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var s = new(model.Single)
		if err = cursor.Decode(&s); err != nil {
			return nil, err
		}
		result = append(result, s)
	}

	return result, nil
}

func (s *TournamentStore) FindAllSinglesByAdmin(admin string) (map[string]string, error) {
	mySingles := make(map[string]string)

	ctx := context.TODO()
	filter := bson.D{{"admin", admin}}

	cursor, err := s.Single.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	var single = new(model.Single)
	for cursor.Next(ctx) {
		if err = cursor.Decode(&single); err != nil {
			return nil, err
		}
		mySingles[single.ID.Hex()] = single.Name
	}

	return mySingles, nil
}
