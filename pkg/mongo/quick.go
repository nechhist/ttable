package mongo

import (
	"errors"
)

var (
	ErrQuickNotFound     = errors.New("quick not found")
)
