package mongo

import (
	"context"
	"errors"

	"gitlab.com/nechhist/ttable/model"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	ErrUserNotFound = errors.New("user not found")
)

// Create ...
func (s *UserStore) CreateUser(u *model.User) error {
	_, err := s.User.InsertOne(context.TODO(), u)
	return err
}

// FindByName ...
func (s *UserStore) FindUserByName(name string) (*model.User, error) {
	filter := bson.D{{"name", name}}
	var result *model.User

	err := s.User.FindOne(context.TODO(), filter).Decode(&result)
	return result, err
}

// IsUniqueName ...
func (s *UserStore) IsUniqueUserName(name string) bool {
	_, err := s.FindUserByName(name)
	return err != nil
}
