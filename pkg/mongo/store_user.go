package mongo

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// UserStore
type UserStore struct {
	User     *mongo.Collection
	Feedback *mongo.Collection
	// users map[int]*model.User // cache ??
}

type UserConfig struct {
	NameDB             string
	CollectionUser     string
	CollectionFeedback string
}

func (cfg UserConfig) Valid() error {
	if cfg.NameDB == "" {
		return errors.New("NameDB is empty")
	}
	if cfg.CollectionUser == "" {
		return errors.New("CollectionUser is empty")
	}
	if cfg.CollectionFeedback == "" {
		return errors.New("CollectionFeedback is empty")
	}
	return nil
}

func NewUserStore(mongoURL string, cfg UserConfig) (*UserStore, error) {
	if err := cfg.Valid(); err != nil {
		return nil, err
	}

	clientOptions := options.Client().ApplyURI(mongoURL)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, err
	}

	db := client.Database(cfg.NameDB)

	// collections
	store := &UserStore{
		User:     db.Collection(cfg.CollectionUser),
		Feedback: db.Collection(cfg.CollectionFeedback),
	}

	// index
	_, err = store.User.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.M{
				"name": 1,
			},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		return nil, err
	}

	return store, nil
}
