package mongo

import (
	"context"
	"errors"

	"gitlab.com/nechhist/ttable/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ErrTournamentNotFound = errors.New("tournament not found")
	ErrSeasonNotFound     = errors.New("season not found")
)

func (s *TournamentStore) CreateTournament(tournament *model.Tournament) error {
	ctx := context.TODO()

	id, err := s.getLastID()
	if err != nil {
		return err
	}

	tournament.ID = id
	_, err = s.Tournament.InsertOne(ctx, tournament)

	return err
}

func (s *TournamentStore) UpdateTournament(tournament *model.Tournament) error {
	ctx := context.TODO()
	filter := bson.D{{"id", tournament.ID}}
	_, err := s.Tournament.ReplaceOne(ctx, filter, tournament)

	return err
}

func (s *TournamentStore) FindTournamentByID(id uint64) (*model.Tournament, error) {
	filter := bson.D{{"id", id}}
	var result *model.Tournament

	err := s.Tournament.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (s *TournamentStore) FindAllTournaments(offset, limit int64, field string, desc bool) ([]*model.Tournament, error) {
	result := make([]*model.Tournament, 0)
	option := &options.FindOptions{
		Limit: &limit,
		Skip:  &offset,
	}

	if field == "" {
		sort := 0
		if desc {
			sort = -1
		}
		option = &options.FindOptions{
			Limit: &limit,
			Skip:  &offset,
			Sort:  bson.D{{field, sort}},
		}
	}

	ctx := context.TODO()
	cursor, err := s.Tournament.Find(ctx, bson.D{}, option)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var t = new(model.Tournament)
		if err = cursor.Decode(&t); err != nil {
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (s *TournamentStore) FindAllTournamentsByAdmin(admin string) (map[uint64]string, error) {
	myTournaments := make(map[uint64]string)
	ctx := context.TODO()
	filter := bson.D{{"admin", admin}}

	cursor, err := s.Tournament.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	var t = new(model.Tournament)
	for cursor.Next(ctx) {
		if err = cursor.Decode(&t); err != nil {
			return nil, err
		}
		myTournaments[t.ID] = t.Name
	}

	return myTournaments, nil
}

// LAST_ID //////////////////////////////////////////////////////////////////////
type LastID struct {
	LastID uint64 `json:"id" bson:"last_id"`
}

var keyID = "_id"
var valueID = "tournament"
var keyLastID = "last_id"

func (s *TournamentStore) getLastID() (uint64, error) {
	ctx := context.Background()
	var lastId LastID
	filter := bson.D{{keyID, valueID}}
	update := bson.M{"$inc": bson.D{{keyLastID, 1}}}
	err := s.LastID.FindOneAndUpdate(ctx, filter, update).Decode(&lastId)
	return lastId.LastID + 1, err
}
