package mongo

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/nechhist/ttable/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoStore
type TournamentStore struct {
	Tournament *mongo.Collection
	LastID     *mongo.Collection
	Single     *mongo.Collection
	Quick      *mongo.Collection
	// users map[int]*model.User // cache ??
}

type TournamentConfig struct {
	NameDB               string
	CollectionTournament string
	CollectionLastID     string
	CollectionSingle     string
	CollectionQuick      string
}

func (cfg TournamentConfig) Valid() error {
	if cfg.NameDB == "" {
		return errors.New("NameDB is empty")
	}
	if cfg.CollectionTournament == "" {
		return errors.New("CollectionTournaments is empty")
	}
	if cfg.CollectionLastID == "" {
		return errors.New("CollectionLastID is empty")
	}
	if cfg.CollectionSingle == "" {
		return errors.New("CollectionSingle is empty")
	}
	if cfg.CollectionQuick == "" {
		return errors.New("CollectionQuick is empty")
	}
	return nil
}

func NewTournamentStore(mongoURL string, cfg TournamentConfig) (*TournamentStore, error) {
	if err := cfg.Valid(); err != nil {
		return nil, err
	}

	clientOptions := options.Client().ApplyURI(mongoURL)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, err
	}

	db := client.Database(cfg.NameDB)

	// collections
	store := &TournamentStore{
		Tournament: db.Collection(cfg.CollectionTournament),
		LastID:     db.Collection(cfg.CollectionLastID),
		Single:     db.Collection(cfg.CollectionSingle),
		Quick:      db.Collection(cfg.CollectionQuick),
	}

	// index
	_, err = store.Tournament.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.M{
				"id": 1,
			},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		return nil, err
	}

	_, err = store.Tournament.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.M{
				"admin": 1,
			},
		},
	)
	if err != nil {
		return nil, err
	}

	_, err = store.Single.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.M{
				"admin": 1,
			},
		},
	)
	if err != nil {
		return nil, err
	}

	_, err = store.Quick.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys: bson.M{
				"admin": 1,
			},
		},
	)
	if err != nil {
		return nil, err
	}

	return store, nil
}

func (s *TournamentStore) InitMigration() error {
	ctx := context.Background()
	lastTournamentID := uint64(0)

	// проверяем что есть таблица "tournaments"
	countT, err := s.Tournament.CountDocuments(ctx, bson.D{})
	if err != nil {
		return err
	}

	if countT != 0 {
		// если таблица "tournaments" заполнена - бурем последний id
		var t model.Tournament
		err = s.Tournament.FindOne(ctx, bson.D{}, &options.FindOneOptions{Sort: bson.D{{"id", -1}}}).Decode(&t)
		if err != nil {
			return err
		}
		lastTournamentID = t.ID
	}

	// проверяем что есть таблица "last_id c _id = tournament"
	var l LastID
	if err = s.LastID.FindOne(ctx, bson.D{{keyID, valueID}}).Decode(&l); err != nil {
		// no documents in result
		_, err := s.LastID.InsertOne(
			ctx,
			bson.D{{keyID, valueID}, {keyLastID, lastTournamentID}},
		)
		if err != nil {
			return err
		}
	} else {
		// документ найден - проверим LastID
		if l.LastID != lastTournamentID {
			return fmt.Errorf("not equal LastID %v with lastTournamentID %v", l.LastID, lastTournamentID)
		}
	}

	return nil
}
