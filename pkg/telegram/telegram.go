package telegram

import (
	"net/http"
)

// Bot ...
type Bot struct {
	tokenBot string
	chatID   string
	prefix   string
}

// Config ...
type Config struct {
	TokenBot string
	ChatID   string
	Prefix   string
}

// NewBot ...
func NewBot(cfg Config) *Bot {
	return &Bot{
		tokenBot: cfg.TokenBot,
		chatID:   cfg.ChatID,
		prefix:   cfg.Prefix,
	}
}

// SendMessage ...
func (b *Bot) SendMessage(message string) error {
	if b == nil {
		return nil
	}
	var url = "https://api.telegram.org/bot" + b.tokenBot + "/sendMessage?chat_id=" + b.chatID + "&text=" + b.prefix + ": " + message
	_, err := http.Get(url)
	return err
}

const (
	// TokenBotDefault ...
	TokenBotDefault = "184883614:AAHAthCjIqTcNPNL0TEVOXkEeDxdmANs-ac"
	// ChatIDDefault ...
	ChatIDDefault = "-1001060781892"
)

// SendMessageDefault ...
func SendMessageDefault(message string) error {
	var url = "https://api.telegram.org/bot" + TokenBotDefault + "/sendMessage?chat_id=" + ChatIDDefault + "&text=" + message
	_, err := http.Get(url)
	return err
}
