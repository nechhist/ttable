package user

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/sessions"
	"gitlab.com/nechhist/ttable/pkg/antispam"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/shutdown"
	"gitlab.com/nechhist/ttable/pkg/telegram"
)

var (
	antiSpamDelaySec int64 = 60
	tokenTTL         int64 = 60 * 60 * 24
)

// Service ...
type Service struct {
	config       *Config
	httpServer   *http.Server
	logger       log.Logger
	userStore    Store
	sessionStore sessions.Store
	spamStore    *antispam.Store
	telegram     *telegram.Bot
}

// New ...
func New(cfg *Config, store Store, sessionStore sessions.Store, telegramBot *telegram.Bot, logger log.Logger) *Service {
	s := &Service{}
	s.userStore = store
	s.sessionStore = sessionStore
	s.logger = logger
	s.telegram = telegramBot
	s.config = cfg
	s.httpServer = &http.Server{
		Addr:         fmt.Sprintf(":%v", cfg.ServerPort),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      s.getHandlers(),
	}

	// spamStore
	s.spamStore = antispam.NewStore(antiSpamDelaySec)
	go s.spamStore.RunGC()

	return s
}

// ListenAndServe ...
func (s *Service) ListenAndServe() error {
	return s.httpServer.ListenAndServe()
}

// Shutdown ...
func (s *Service) Shutdown() error {
	return shutdown.Shutdown(s.httpServer, 2*time.Second)
}
