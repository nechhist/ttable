package user

import (
	"context"
	"fmt"
	"net/http"
	"runtime/debug"
	"time"
)

const ctxUser int8 = 0

type responseWriter struct {
	http.ResponseWriter
	code int
}

func (s *Service) errorMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			l := s.logger.With(r.Context())
			if e := recover(); e != nil {
				var ok bool
				var err error
				if err, ok = e.(error); !ok {
					err = fmt.Errorf("%v", e)
				}
				if err != nil {
					s.response(w, r, http.StatusInternalServerError, err, "errorMiddleware")
				}
				l.Errorf("recovered from panic (%v): %s", err, debug.Stack())
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func (s *Service) accessMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		s.logger.Infof(
			"Access Log: %s %s %s %d %dms",
			r.Method,
			r.URL.Path,
			r.Proto,
			rw.code,
			time.Since(start).Milliseconds(),
		)
	})
}

func (s *Service) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := s.sessionStore.Get(r, SessionCookieName)
		if err != nil {
			s.logger.Debug("authenticationMiddleware sessionStore.Get", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "authenticationMiddleware/sessionStore.Get")
			return
		}

		id, ok := session.Values["user_id"]
		if !ok {
			s.logger.Debug("authenticationMiddleware session.Values", id, http.StatusUnauthorized, errNotAuthenticated)
			s.response(w, r, http.StatusUnauthorized, errNotAuthenticated, "authenticationMiddleware/session.Values")
			return
		}

		u, err := s.userStore.FindUserByName(id.(string))
		if err != nil {
			s.logger.Debug("authenticationMiddleware FindByID", id, http.StatusUnauthorized, errNotAuthenticated)
			s.response(w, r, http.StatusUnauthorized, errNotAuthenticated, "authenticationMiddleware/FindByID")
			return
		}
		//s.response(w, r, http.StatusOK, u)
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), ctxUser, u)))
	})
}
