package user

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/sessions"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nechhist/ttable/pkg/mongo"

	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/antispam"
	log "gitlab.com/nechhist/ttable/pkg/logger"
	"gitlab.com/nechhist/ttable/pkg/telegram"

	"github.com/gorilla/securecookie"
	"gitlab.com/nechhist/ttable/pkg/token"
)

var (
	sessionKeyTest = []byte("secret")
)

// TestMongo ///////////////////////////////////////////
func Test_MongoService(t *testing.T) {
	// 1
	HandleRegistrationCode(t, getTestMongoServer(t))
	HandleRegistrationResponse(t, getTestMongoServer(t))
	// 2
	HandleAuthorizationCode(t, getTestMongoServer(t))
	HandleAuthorizationResponse(t, getTestMongoServer(t))
	// 3
	HandleIdentificationCode(t, getTestMongoServer(t))
	HandleIdentificationResponse(t, getTestMongoServer(t))
}

// Registration ///////////////////////////////////
func HandleRegistrationCode(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}

	if err := testUser.BeforeCreate(); err != nil {
		t.Fatal(err)
	}
	if err := s.userStore.CreateUser(testUser); err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload RegistrationRequest
		code    int
	}{
		{
			name: "ok",
			payload: RegistrationRequest{
				Username: "name_" + strconv.Itoa(int(time.Now().UnixNano())), // not unique with username
				Email:    testUser.Email,
				Password: "Password",
			},
			code: http.StatusCreated,
		},
		{
			name: "fail - unique name",
			payload: RegistrationRequest{
				Username: testUser.Name,
				Email:    testUser.Email,
				Password: "Password",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			name: "fail - short name",
			payload: RegistrationRequest{
				Username: "x",
				Email:    testUser.Email,
				Password: "Password",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			name: "fail - short password",
			payload: RegistrationRequest{
				Username: testUser.Name,
				Email:    testUser.Email,
				Password: "x",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			name: "fail - empty name ",
			payload: RegistrationRequest{
				Username: "",
				Email:    testUser.Email,
				Password: "Password",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			name: "fail - empty pass ",
			payload: RegistrationRequest{
				Username: testUser.Name,
				Email:    testUser.Email,
				Password: "",
			},
			code: http.StatusUnprocessableEntity,
		},
		{
			name: "fail email",
			payload: RegistrationRequest{
				Username: testUser.Name,
				Email:    "invalid",
				Password: "Password",
			},
			code: http.StatusUnprocessableEntity,
		},
	}

	for _, tc := range testCases {
		url := urlAPIv1 + urlRegistration
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body: %v", url, string(htmlData))
		})
	}
}

func HandleRegistrationResponse(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}
	_token, _ := token.Create(testUser.Name, s.config.TokenKey)

	testCases := []struct {
		name     string
		payload  RegistrationRequest
		response UserDataResponse
	}{
		{
			name: "normal",
			payload: RegistrationRequest{
				Username: testUser.Name,
				Password: testUser.Password,
				Email:    testUser.Email,
			},
			response: UserDataResponse{
				Name:  testUser.Name,
				Token: _token,
			},
		},
		{
			name:     "empty cookie",
			payload:  RegistrationRequest{},
			response: UserDataResponse{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, urlAPIv1+urlRegistration, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			response := UserDataResponse{}
			json.NewDecoder(rec.Body).Decode(&response)

			tc.response.SessionID = GetResponseRecorderCookie(t, rec)
			assert.Equal(t, tc.response, response)
		})
	}
}

// Authorization ////////////////////////////////
func HandleAuthorizationCode(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}

	if err := testUser.BeforeCreate(); err != nil {
		t.Fatal(err)
	}
	if err := s.userStore.CreateUser(testUser); err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name    string
		payload interface{}
		code    int
	}{
		{
			name: "ok",
			payload: map[string]interface{}{
				"username": testUser.Name,
				"password": "Password",
			},
			code: http.StatusOK,
		},
		{
			name: "invalid name",
			payload: map[string]interface{}{
				"username": "invalid",
				"password": "Password",
			},
			code: http.StatusUnauthorized,
		},
		{
			name: "invalid password",
			payload: map[string]interface{}{
				"username": testUser.Name,
				"password": "invalid",
			},
			code: http.StatusUnauthorized,
		},
		{
			name:    "invalid payload",
			payload: "invalid",
			code:    http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		url := urlAPIv1 + urlAuthorization
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, url, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body: %v", url, string(htmlData))
		})
	}
}

func HandleAuthorizationResponse(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}

	if err := testUser.BeforeCreate(); err != nil {
		t.Fatal(err)
	}
	if err := s.userStore.CreateUser(testUser); err != nil {
		t.Fatal(err)
	}

	_token, err := token.Create(testUser.Name, s.config.TokenKey)
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		name     string
		payload  AuthorizationRequest
		response UserDataResponse
	}{
		{
			name: "normal",
			payload: AuthorizationRequest{
				Username: testUser.Name,
				Password: "Password",
			},
			response: UserDataResponse{
				Name:  testUser.Name,
				Token: _token,
			},
		},
		{
			name:     "empty cookie",
			payload:  AuthorizationRequest{},
			response: UserDataResponse{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodPost, urlAPIv1+urlAuthorization, b)
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			response := UserDataResponse{}
			json.NewDecoder(rec.Body).Decode(&response)
			tc.response.SessionID = GetResponseRecorderCookie(t, rec)
			assert.Equal(t, tc.response, response)
		})
	}
}

// Identification /////////////////////////////////////

func HandleIdentificationCode(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}

	if err := testUser.BeforeCreate(); err != nil {
		t.Fatal(err)
	}
	if err := s.userStore.CreateUser(testUser); err != nil {
		t.Fatal(err)
	}

	sc := securecookie.New(sessionKeyTest, nil)
	cookieStr, _ := sc.Encode(SessionCookieName, map[interface{}]interface{}{
		"user_id": testUser.Name,
	})

	testCases := []struct {
		name   string
		cookie string
		code   int
	}{
		{
			name:   "ok cookie",
			cookie: cookieStr,
			code:   http.StatusOK,
		},
		{
			name:   "fail cookie",
			cookie: "fail",
			code:   401,
		},
	}

	for _, tc := range testCases {
		url := urlAPIv1 + urlUser + urlIdentification
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, url, nil)

			req.Header.Set("Cookie", fmt.Sprintf("%s=%s", SessionCookieName, tc.cookie))
			s.httpServer.Handler.ServeHTTP(rec, req)
			htmlData, _ := ioutil.ReadAll(rec.Body)
			assert.Equalf(t, tc.code, rec.Code, "url: %v, recorder body: %v", url, string(htmlData))
		})
	}
}

func HandleIdentificationResponse(t *testing.T, s Service) {
	testUser := &model.User{
		Name:     "name_" + strconv.Itoa(int(time.Now().UnixNano())),
		Email:    "test@mail.com",
		Password: "Password",
	}

	if err := testUser.BeforeCreate(); err != nil {
		t.Fatal(err)
	}
	if err := s.userStore.CreateUser(testUser); err != nil {
		t.Fatal(err)
	}

	sc := securecookie.New(sessionKeyTest, nil)
	_token, _ := token.Create(testUser.Name, s.config.TokenKey)

	testCases := []struct {
		name        string
		cookieValue map[interface{}]interface{}
		response    UserDataResponse
	}{
		{
			name: "normal",
			cookieValue: map[interface{}]interface{}{
				"user_id": testUser.Name,
			},
			response: UserDataResponse{
				Name:  testUser.Name,
				Token: _token,
			},
		},
		{
			name:        "empty cookie",
			cookieValue: nil,
			response:    UserDataResponse{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, urlAPIv1+urlUser+urlIdentification, nil)
			cookieStr, _ := sc.Encode(SessionCookieName, tc.cookieValue)
			req.Header.Set("Cookie", fmt.Sprintf("%s=%s", SessionCookieName, cookieStr))
			s.httpServer.Handler.ServeHTTP(rec, req)

			// check body
			response := UserDataResponse{}
			json.NewDecoder(rec.Body).Decode(&response)
			assert.Equal(t, tc.response, response)
		})
	}
}

//  GetCookie ///////////////////////////////
func GetResponseRecorderCookie(t *testing.T, res *httptest.ResponseRecorder) string {
	t.Helper()

	cookies, ok := res.HeaderMap["Set-Cookie"]
	if !ok {
		fmt.Println("no Set-Cookie in res.HeaderMap")
		return ""
	}

	var sessionID string

	for _, str := range cookies {
		// бъем куку на часто по ";"
		split := strings.Split(str, ";")
		// бъем куку на часто по "="
		split = strings.SplitN(split[0], "=", 2)
		if len(split) == 2 && split[0] == SessionCookieName {
			sessionID = split[1]
			break
		}
	}

	return sessionID
}

// MongoServer //////////////////////////////
func getTestMongoServer(t *testing.T) Service {
	t.Helper()
	s := Service{}
	s.logger = log.New(log.GetLevel("debug"))
	s.config = &Config{
		TokenKey:       "test",
		AllowedOrigins: []string{"*"},
	}

	s.sessionStore = sessions.NewCookieStore(sessionKeyTest)

	store, err := mongo.NewUserStore("mongodb://admin:admin@localhost:27017", mongo.UserConfig{
		NameDB:         "test_tt",
		CollectionUser: "tt_user",
	})
	if err != nil {
		t.Fatal(err)
	}

	s.userStore = store

	s.httpServer = &http.Server{
		Handler: s.getHandlers(),
	}
	s.spamStore = antispam.NewStore(0)
	s.telegram = telegram.NewBot(telegram.Config{"", "", ""})

	return s
}
