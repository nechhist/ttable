package user

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/nechhist/ttable/model"
	"gitlab.com/nechhist/ttable/pkg/antispam"
	"gitlab.com/nechhist/ttable/pkg/token"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	// SessionCookieName ...
	SessionCookieName = "session_id"
	// HeaderSessionID ...
	HeaderSessionID = "x-session-id"
	// HeaderUserToken ...
	HeaderUserToken = "x-user-token"
	// HeaderUserName ...
	HeaderUserName = "x-user-name"
)

var (
	//errResponseSpam = fmt.Errorf("too many registrations from this IP address, wait %d sec", antiSpamDelay)
	errResponseSpam         = fmt.Errorf("Слишком много регистраций с данного IP, подождите %d сек.", antiSpamDelaySec)
	errResponseSpamFeedback = fmt.Errorf("Слишком много сообщений с данного IP, подождите %d сек.", antiSpamDelaySec)
	errContextValueUser     = fmt.Errorf("context value ctxUser failed")
	errNotAuthenticated     = errors.New("not authenticated")
	errNotEnoughParameters  = errors.New("not enough parameters")
)

var (
	urlAPIv1 = "/api/v1"

	urlRegistration  = "/registration"
	urlAuthorization = "/authorization"
	urlLogout        = "/logout"

	urlUser           = "/user"
	urlIdentification = "/id"

	urlFeedback = "/feedback"

	headersOk     = handlers.AllowedHeaders([]string{"content-type", "set-cookie", HeaderSessionID, HeaderUserName, HeaderUserToken})
	methodsOk     = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	credentialsOk = handlers.AllowCredentials()
)

// getRouters ...
func (s *Service) getHandlers() *mux.Router {
	router := mux.NewRouter()
	router.Use(handlers.CORS(credentialsOk, headersOk, handlers.AllowedOrigins(s.config.AllowedOrigins), methodsOk))

	router.Handle("/metrics", promhttp.Handler()).Methods("GET")
	router.HandleFunc("/healthcheck", s.handleHealthcheck()).Methods("GET")

	apiV1 := router.PathPrefix(urlAPIv1).Subrouter()
	apiV1.Use(s.accessMiddleware)
	apiV1.Use(s.errorMiddleware)

	// example: curl -i -X POST -d "{\"username\":\"admin\",\"email\":\"email@email.com\",\"password\":\"qwerty\"}" http://localhost:15800/api/v1/registration
	apiV1.HandleFunc(urlRegistration, s.handleRegistration()).Methods("POST")
	// example: curl -i -X POST -d "{\"username\":\"admin\",\"email\":\"email@email.com\",\"password\":\"qwerty\"}" http://localhost:15800/api/v1/authorization
	apiV1.HandleFunc(urlAuthorization, s.handleAuthorization()).Methods("POST")
	// example: curl -i -X GET --cookie "session_id=" http://localhost:15800/api/v1/logout
	apiV1.HandleFunc(urlLogout, s.handleLogout()).Methods("GET")

	user := apiV1.PathPrefix(urlUser).Subrouter()
	user.Use(s.authenticationMiddleware)
	// example: curl -i -X GET --cookie "session_id=MTU4MjgxMTgyNXxEdi1CQkFFQ180SUFBUkFCRUFBQUh2LUNBQUVHYzNSeWFXNW5EQWtBQjNWelpYSmZhV1FEYVc1MEJBSUFBZz09fJogZLEL6MHP_kLtZpZYMLfEvhdSAFDJaXyqhBbIpC8P" http://localhost:15800/api/v1/user/id
	user.HandleFunc(urlIdentification, s.handleIdentification()).Methods("GET")

	// feedback
	apiV1.HandleFunc(urlFeedback, s.handleFeedbackGetAll()).Methods("GET")
	apiV1.HandleFunc(urlFeedback, s.handleFeedbackCreate()).Methods("POST")

	// DUMMY /////////////////////////////////////////////////////////////////////////////////////
	apiV1.HandleFunc(urlFeedback, handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc(urlRegistration, handleDummy).Methods("OPTIONS")
	apiV1.HandleFunc(urlAuthorization, handleDummy).Methods("OPTIONS")

	return router
}

// handleDummy
func handleDummy(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello Dummy!")
}

// UserDataResponse ...
type UserDataResponse struct {
	Name      string
	Token     string
	SessionID string
}

func (s *Service) handleIdentification() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u, ok := r.Context().Value(ctxUser).(*model.User)
		if !ok {
			s.logger.Error("handleIdentification Context().Value error:", http.StatusInternalServerError, errContextValueUser)
			s.response(w, r, http.StatusInternalServerError, errContextValueUser, "Identification/Context.Value")
			return
		}

		_token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleIdentification token create error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Identification/token.Create")
			return
		}
		// w.Header().Add(HeaderSessionID, "")
		// w.Header().Add(HeaderUserName, u.Name)
		// w.Header().Add(HeaderUserToken, token)
		s.response(w, r, http.StatusOK, UserDataResponse{u.Name, _token, ""}, "Identification")
	}
}

// RegistrationRequest ...
type RegistrationRequest struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// handleRegistration ...
func (s *Service) handleRegistration() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// antispam
		if err := s.spamStore.Add(r.RemoteAddr); err != nil {
			if err == antispam.ErrIPExists {
				s.logger.Info("handleRegistration antispam add ErrIPExists error:", http.StatusUnprocessableEntity, errResponseSpam)
				s.response(w, r, http.StatusUnprocessableEntity, errResponseSpam, "Registration/ErrIPExists")
			} else {
				s.logger.Info("handleRegistration antispam add error:", http.StatusUnprocessableEntity, err)
				s.response(w, r, http.StatusUnprocessableEntity, err, "Registration/spamStore.Add")
			}
			return
		}

		req := &RegistrationRequest{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.logger.Debug("handleRegistration json NewDecoder error:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "Registration/json.NewDecoder")
			return
		}

		u := &model.User{
			Name:     req.Username,
			Email:    req.Email,
			Password: req.Password,
		}

		if err := u.Validate(); err != nil {
			s.logger.Info("handleRegistration user validate error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "Registration/Validate")
			return
		}

		if err := u.BeforeCreate(); err != nil {
			s.logger.Error("handleRegistration BeforeCreate error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "Registration/BeforeCreate")
			return
		}

		if !s.userStore.IsUniqueUserName(u.Name) {
			s.logger.Debug("handleRegistration userStore IsUniqueName error:", http.StatusUnprocessableEntity)
			s.response(w, r, http.StatusUnprocessableEntity, "is not unique name", "Registration/IsUniqueName")
			return
		}

		if err := s.userStore.CreateUser(u); err != nil {
			s.logger.Error("handleRegistration userStore Create error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Registration/Create")
			return
		}

		// ser cookie to w
		if err := s.setCookie(u, w, r); err != nil {
			s.logger.Error("handleAuthorization setCookie error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Registration/setCookie")
			return
		}

		// get cookie from w
		sessionID, err := getSessionIDFromCookie(w)
		if sessionID == "" {
			s.logger.Error("handleRegistration getSessionIDFromCookie error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Registration/getSessionIDFromCookie")
			return
		}

		_token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleRegistration token Create error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Registration/token.Create")
			return
		}

		// w.Header().Add(HeaderSessionID, sessionID)
		// w.Header().Add(HeaderUserName, u.Name)
		// w.Header().Add(HeaderUserToken, token)

		go s.telegram.SendMessage("Create user: " + u.Name)
		s.response(w, r, http.StatusCreated, UserDataResponse{u.Name, _token, sessionID}, "Registration")
	}
}

// AuthorizationRequest ...
type AuthorizationRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// handleAuthorization ...
func (s *Service) handleAuthorization() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &AuthorizationRequest{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.logger.Debug("handleAuthorization json Decode error:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "Authorization/json.NewDecoder")
			return
		}

		u, err := s.userStore.FindUserByName(req.Username)
		if err != nil {
			s.logger.Debug("handleAuthorization userStore FindByName error:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, ErrIncorrectNameOrPassword, "Authorization/FindByName")
			return
		}
		if !u.ComparePassword(req.Password) {
			s.logger.Debug("handleAuthorization user comparePassword error:", http.StatusUnauthorized, ErrIncorrectNameOrPassword)
			s.response(w, r, http.StatusUnauthorized, ErrIncorrectNameOrPassword, "Authorization/ComparePassword")
			return
		}

		if err := s.setCookie(u, w, r); err != nil {
			s.logger.Debug("handleAuthorization setCookie error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Authorization/setCookie")
			return
		}

		sessionID, err := getSessionIDFromCookie(w)
		if sessionID == "" {
			s.logger.Error("handleRegistration getSessionIDFromCookie error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Authorization/getSessionIDFromCookie")
			return
		}

		_token, err := token.Create(u.Name, s.config.TokenKey)
		if err != nil {
			s.logger.Error("handleAuthorization token create error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Authorization/token.Create")
			return
		}

		// w.Header().Add(HeaderSessionID, sessionID)
		// w.Header().Add(HeaderUserName, u.Name)
		// w.Header().Add(HeaderUserToken, token)
		s.response(w, r, http.StatusOK, UserDataResponse{u.Name, _token, sessionID}, "Authorization")
	}
}

func (s *Service) handleLogout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session, err := s.sessionStore.Get(r, SessionCookieName)
		if err != nil {
			s.logger.Debug("handleLogout sessionStore Get error:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "Logout/sessionStore.Get")
			return
		}

		session.Options.MaxAge = -1
		if err := s.sessionStore.Save(r, w, session); err != nil {
			s.logger.Error("handleLogout sessionStore Save error:", http.StatusInternalServerError, err)
			s.response(w, r, http.StatusInternalServerError, err, "Logout/sessionStore.Save")
			return
		}
		// w.Header().Add(HeaderSessionID, "")
		// w.Header().Add(HeaderUserName, "")
		// w.Header().Add(HeaderUserToken, "")
		s.response(w, r, http.StatusOK, nil, "Logout")
	}
}

// FeedbackRequest ...
type FeedbackRequest struct {
	Token string `json:"token"`
	Name  string `json:"name"`
	Text  string `json:"text"`
}

func (s *Service) handleFeedbackCreate() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// antispam
		if err := s.spamStore.Add(r.RemoteAddr); err != nil {
			if err == antispam.ErrIPExists {
				s.logger.Info("handleFeedbackCreate anti_spam add ErrIPExists error:", http.StatusUnprocessableEntity, errResponseSpamFeedback)
				s.response(w, r, http.StatusUnprocessableEntity, errResponseSpamFeedback, "handleFeedbackCreate/spamStore")
			} else {
				s.logger.Info("handleFeedbackCreate anti_spam add error:", http.StatusUnprocessableEntity, err)
				s.response(w, r, http.StatusUnprocessableEntity, err, "handleFeedbackCreate/spamStore")
			}
			return
		}

		// get json
		var feedbackRequest FeedbackRequest
		if err := json.NewDecoder(r.Body).Decode(&feedbackRequest); err != nil {
			s.logger.Info("handleFeedbackCreate json Decode:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleFeedbackCreate/json.Decode")
			return
		}

		if feedbackRequest.Name == "" || feedbackRequest.Token == "" {
			s.logger.Error("handleFeedbackCreate:", http.StatusBadRequest, errNotEnoughParameters)
			s.response(w, r, http.StatusBadRequest, errNotEnoughParameters, "handleFeedbackCreate/empty")
			return
		}

		// check secret
		if err := s.checkSecret(feedbackRequest.Name, feedbackRequest.Token); err != nil {
			s.logger.Error("handleFeedbackCreate checkSecret:", http.StatusUnauthorized, err)
			s.response(w, r, http.StatusUnauthorized, err, "handleFeedbackCreate/checkSecret")
			return
		}

		feedback := &model.Feedback{}
		feedback.Name = feedbackRequest.Name
		feedback.Text = feedbackRequest.Text
		feedback.BeforeCreate()
		if err := feedback.Validate(); err != nil {
			s.logger.Error("handleFeedbackCreate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleFeedbackCreate/BeforeCreate")
			return
		}

		if err := s.userStore.CreateFeedback(feedback); err != nil {
			s.logger.Error("handleFeedbackCreate createSingle:", http.StatusUnprocessableEntity, err)
			s.response(w, r, http.StatusUnprocessableEntity, err, "handleFeedbackCreate/createSingle")
			return
		}

		go s.telegram.SendMessage("Create Feedback: " + feedback.Name)

		// get all
		feedbacks, err := s.userStore.FindAllFeedback(0, 500)
		if err != nil {
			s.logger.Error("handleFeedbackCreate Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleFeedbackCreate/BeforeCreate")
			return
		}

		s.response(w, r, http.StatusCreated, feedbacks, "handleFeedbackCreate")
	}
}

func (s *Service) handleFeedbackGetAll() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		feedbacks, err := s.userStore.FindAllFeedback(0, 500)
		if err != nil {
			s.logger.Error("handleFeedbackGetAll Validate:", http.StatusBadRequest, err)
			s.response(w, r, http.StatusBadRequest, err, "handleFeedbackGetAll/BeforeCreate")
			return
		}

		s.response(w, r, http.StatusOK, feedbacks, "handleFeedbackGetAll")
	}
}

// handleHealthcheck ...
func (s *Service) handleHealthcheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// check DB
		data := struct{ Status string }{"ok"}
		s.response(w, r, http.StatusOK, data, "")
	}
}

// setCookie ...
func (s *Service) setCookie(u *model.User, w http.ResponseWriter, r *http.Request) error {
	session, _ := s.sessionStore.Get(r, SessionCookieName)

	session.Values["user_id"] = u.Name
	if err := s.sessionStore.Save(r, w, session); err != nil {
		return err
	}

	return nil
}

// getSessionIDFromCookie ...
func getSessionIDFromCookie(w http.ResponseWriter) (string, error) {
	var sessionID string
	var err error

	if cookies := w.Header().Get("Set-Cookie"); cookies != "" {
		// бъем куку на часто по ";"
		split := strings.Split(string(cookies), ";")
		// бъем куку на часто по "="
		split = strings.SplitN(split[0], "=", 2)
		if len(split) == 2 && split[0] == SessionCookieName {
			sessionID = split[1]
		}
	}

	if sessionID == "" {
		err = errors.New("cookie session_id not set")
	}

	return sessionID, err
}

var metricRequests = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Total number of HTTP requests by status code end method.",
	},
	[]string{"handler", "code"},
)
var metricLabels = prometheus.Labels{}

// response ...
func (s *Service) response(w http.ResponseWriter, r *http.Request, code int, data interface{}, handlerName string) {
	if handlerName != "" {
		metricLabels["code"] = strconv.Itoa(code)
		metricLabels["handler"] = handlerName
		metricRequests.With(metricLabels).Inc()
	}

	w.WriteHeader(code)
	if err, ok := data.(error); ok {
		data = map[string]string{"error": err.Error()}
	}
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
	//s.logger.Debug("Response. Code:", code, ". Header: ", w.Header(), ". Data: ", data)
}

func (s *Service) checkSecret(admin, requestToken string) error {
	payload, err := token.GetPayload(requestToken, s.config.TokenKey, tokenTTL)
	if err != nil {
		return err
	}

	if payload != admin {
		return errors.New("token payload != admin")
	}
	return nil
}
