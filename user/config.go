package user

import (
	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/kelseyhightower/envconfig"
)

// Config represents an application configuration.
type Config struct {
	// LogLevel ...
	LogLevel string
	// ServerPort ...
	ServerPort string
	// AllowedOrigins ...
	AllowedOrigins []string
	// MongoURI
	MongoURL string
	// SessionKey ...
	SessionKey string
	// TokenKey ...
	TokenKey string
	// TelegramTokenBot ...
	TelegramTokenBot string
	// ChatID ...
	TelegramChatID string
}

// LoadConfig returns an application configuration.
func LoadConfig() (*Config, error) {
	c := &Config{
		LogLevel:         "debug",
		ServerPort:       "15800",
		TelegramTokenBot: "184883614:AAHAthCjIqTcNPNL0TEVOXkEeDxdmANs-ac",
		TelegramChatID:   "-1001060781892",

		AllowedOrigins: []string{"http://localhost:3000"},
		MongoURL:       "mongodb://admin:admin@localhost:27017",
		SessionKey:     "session_key",
		TokenKey:       "token_key",
	}
	envconfig.Process("app", c)

	if err := c.Validate(); err != nil {
		return nil, err
	}

	return c, nil
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		// server
		validation.Field(&c.ServerPort, validation.Required),
		// AllowedOrigins
		validation.Field(&c.AllowedOrigins, validation.Required),
		// MongoURL
		validation.Field(&c.MongoURL, validation.Required),
		// Session
		validation.Field(&c.SessionKey, validation.Required),
		validation.Field(&c.TokenKey, validation.Required),
		// Telegram
		validation.Field(&c.TelegramTokenBot, validation.Required),
		validation.Field(&c.TelegramChatID, validation.Required),
	)
}
