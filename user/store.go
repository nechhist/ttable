package user

import (
	"errors"

	"gitlab.com/nechhist/ttable/model"
)

var (
	// ErrUserNameNotUnique = errors.New("user name not unique")
	// ErrIncorrectNameOrPassword = errors.New("incorrect name or password")
	ErrIncorrectNameOrPassword = errors.New("Неверная комбинация имя/пароль.")
)

// Store ...
type Store interface {
	CreateUser(u *model.User) error
	FindUserByName(name string) (*model.User, error)
	IsUniqueUserName(name string) bool

	CreateFeedback(u *model.Feedback) error
	FindAllFeedback(offset, limit int64) ([]*model.Feedback, error)
}
