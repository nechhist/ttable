admin Ma2144
........................................
docker run -d -p15709:27017 \
-e MONGO_INITDB_ROOT_USERNAME=admin \
-e MONGO_INITDB_ROOT_PASSWORD=65fd7fb08432f8 \
-v /tt/go/src/data/mongo:/data/db \
mongo

.............................................................

docker build -f cmd/user/Dockerfile -t user .
docker build -f cmd/tournament/Dockerfile -t tournament .
docker build -f cmd/counter/Dockerfile -t counter .
docker build -f cmd/websocket/Dockerfile -t websocket .

..............................................................

docker run -d \
-e APP_MONGOURL="mongodb://admin:admin@localhost:15709" \
-e APP_ALLOWEDORIGINS="http://ttable.pro" \
-e APP_SESSIONKEY="4cfdcb36ee70014e" \
-e APP_TOKENKEY="534288ae" \
-e APP_SERVERPORT=15801 \
-v /tt/go/src/data/user:/app/data \
--network host \
user

docker run -d \
-e APP_MONGOURL="mongodb://admin:admin@localhost:15709" \
-e APP_ALLOWEDORIGINS="http://ttable.pro" \
-e APP_TOKENKEY="534288ae" \
-e APP_WEBSOCKETADDR="http://194.67.104.179:15804/api/v1" \
-e APP_SERVERPORT=15802 \
-v /tt/go/src/data/tournament:/app/data \
--network host \
tournament

docker run -d \
-e APP_ALLOWEDORIGINS="http://ttable.pro" \
-e APP_TOKENKEY="534288ae" \
-e APP_SERVERPORT=15803 \
-v /tt/go/src/data/counter:/app/data \
--network host \
counter

docker run -d \
-e APP_ALLOWEDORIGINS="http://ttable.pro" \
-e APP_TOKENKEY="534288ae" \
-e APP_SERVERPORT=15804 \
-v /tt/go/src/data/websocket:/app/data \
--network host \
websocket

...............................................................

docker run -p 80:80 -d \
 -v /tt/go/src/data/nginx/templates:  \
 -v /tt/go/src/data/nginx/nginx.conf:/etc/nginx/conf.d \
 nginx

...............................................................
...............................................................

server {
    listen 80;
    server_name ttable.pro;
    return 301 https://$server_name$request_uri;
}

server {
	listen 443 ssl;
	server_name ttable.pro;
	ssl_certificate /etc/nginx/conf.d/ttable.pro.crt;
	ssl_certificate_key /etc/nginx/conf.d/ttable.pro.key;

	root /usr/share/nginx/html;

	location /service_user/ {
		proxy_pass http://194.67.104.179:15801/;
	}

	location /service_tournament/ {
		proxy_pass http://194.67.104.179:15802/;
	}

	location /service_counter/ {
		proxy_pass http://194.67.104.179:15803/;
	}

	location /service_websocket/ {
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_pass http://194.67.104.179:15804/;
		proxy_read_timeout 600s;
	}

	location / {
		try_files $uri $uri/ /index.html;
	}
}

